<?php
/*
  Plugin Name: LCA konkurs
  Description: -
  Author: OX MEDIA
  Author URI: http://oxmedia.pl/
  Version: 1.0.0
 */
  

 function lca_contest_form(){
   $content = $_POST['content'];
   $statute_checkbox = $_POST['statute'];
   $cardNumber = $_POST['card_number'];
   $image = $_FILES['image'];
   $alert = '';
   $alertok = false;
   if( isset($content) && 
       !is_null($content) && 
       isset($cardNumber) && 
       !is_null($cardNumber) && 
       isset($statute_checkbox) && 
       !is_null($statute_checkbox) && 
       $image['size'] > 0
       ){
      if($image['size'] <= 10485760){
        if( !user_duplicate() ){
          if($image['type'] == "image/jpg" || $image['type'] == "image/png" || $image['type'] == "image/jpeg" || $image['type'] == "image/gif"){
    
            $post_id = get_following_post_id();
    
            $file_ext = substr($image['name'], strripos($image['name'], '.'));
            if(!file_exists("./wp-content/uploads/contest_images")){
              mkdir('./wp-content/uploads/contest_images');
            }
    
            move_uploaded_file($image["tmp_name"], "./wp-content/uploads/contest_images/image_" . $post_id . strtolower($file_ext));
    
            $my_post = array(
              'post_title' => "Zgłoszenie " . $post_id,
              'post_content' => $content,
              'post_status' => 'private',
              'post_type' => 'zgloszenia',
              'page_template'  => 'single-zgloszenia.php'
            );
            $the_post_id = wp_insert_post( $my_post );
            
            add_post_meta( $the_post_id, "Numer karty LCA", $cardNumber );
            add_post_meta( $the_post_id, "IP", $_SERVER['REMOTE_ADDR'] );
            add_post_meta( $the_post_id, "unique_user", $_COOKIE['unique_user'] );
            Generate_Featured_Image("./wp-content/uploads/contest_images/image_" . $post_id . strtolower($file_ext), $the_post_id);
            $alert = "Dziękujemy za Twoje zgłoszenie! Niebawem pojawi się ono w galerii zdjęć.";
            $alertok = true;
          }else{
            //wrong image format
            $alert = "Błąd! Zły format zdjęcia. Dozwolone formaty: JPG, PNG, GIF";
          }
        }else{
          //duplicate
          $alert = "Błąd! Można wysłać tylko jedno zgłoszenie";
        }
      }else{
        $alert = "Błąd! Maksymalny rozmiar zdjęcia nie powinien przekraczać 10MB";
      }
   }
   ?>
    <form id="contestForm" action="#send" method="post" enctype="multipart/form-data" accept="image/*">
      <div class="page-travel__signUp-form-card-wrapper">
        <div class="page-travel__signUp-form-card-header">
          <span class="page-travel__heading page-travel__heading--white page-travel__steps">Wpisz swój numer karty Le Club AccorHotels:</span>
        </div>
        <input type="text" name="card_number" class="page-travel__signUp-form-input page-travel__heading page-travel__heading--white" required>
      </div>
      <div class="page-travel__signUp-form-image page-travel__signUp-form-box-border">
        <img class="icon" src="<?php echo get_template_directory_uri() ?>/img/icons/image.png">
        <div class="page-travel__signUp-form-image-button">
          <input type="file" name="image" value="Wybierz zdjęcie" id="formChooseImage" required>
          <a class="page-travel__button page-travel__button--yellow" id="formImageButton">Wybierz zdjęcie</a>
        </div>
        <img class="pre-image" id="formPreviewImage">
      </div>
      <div class="page-travel__signUp-form-msg-wrapper">
        <div class="page-travel__signUp-form-msg-header">
          <span class="page-travel__heading page-travel__heading--white page-travel__steps">Pytanie konkursowe:</span>
          <span class="page-travel__heading page-travel__heading--white page-travel__steps">dlaczego lubisz podróżować z Le Club AccorHotels?</span>
        </div>
        <textarea id="send" class="page-travel__heading page-travel__heading--white page-travel__heading--textbox page-travel__signUp-form-text-box page-travel__signUp-form-box-border" name="content" placeholder="Wpisz tutaj swoją odpowiedź" required></textarea>
      </div>
      <div class="page-travel__signUp-form-send-wrapper">
        <div class="page-travel__signUp-form-checkbox-wrapper">
          <input id="contestStatute" type="checkbox" name="statute" required>
          <div class="page-travel__signUp-form-custom-checkbox">
            <img src="<?php echo get_template_directory_uri() ?>/img/icons/checkbox.png" alt="">
          </div>
          <span class="page-travel__heading page-travel__heading--statute page-travel__heading--white">Przeczytałem i zaakceptowałem <a href="http://wiecejniz10.pl/wp-content/uploads/2018/08/2018.06-Regulamin-Konkursu-na-10-urodziny-v8.pdf" target="_blank">regulamin</a> konkursu</span>
        </div>
        <input class="page-travel__button page-travel__button--yellow" type="submit" value="Wyślij zgłoszenie">
      </div>
      <?php if( !empty($alert) ): ?>
        <div id="status" class="page-travel__signUp-form-status page-travel__heading page-travel__heading--white"><?php echo $alert; ?></div>
        <?php if($alertok): ?>
          <input type="hidden" id="postSend" value="true">
        <?php endif; ?>
      <?php endif; ?>
    </form>
 <?php }

 function lca_contest_shortcode() {
  ob_start();
  lca_contest_form();

  return ob_get_clean();
}

add_shortcode( 'lca_contest', 'lca_contest_shortcode' );