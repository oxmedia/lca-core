<?php /* Template Name: Podróż 05 - wydarzenia */ ?>
<?php
  get_header();
?>
    <section class="page-events">
    <?php include('includes/prizes-popup.php'); ?>
        <div class='page-events__top'>
            <span class="page-events__heading">Na jakie z minionych wydarzeń wybrałbyś się z nami?</span>
        </div>
        <div class="page-events__bottom">
            <a href="<?php echo get_home_url(); ?>/podroz-film">Osobiście poczuj korzyści podróżowania z Le Club AccorHotel</a>
        </div>
        <div class='page-events__videos'>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="<?php echo get_template_directory_uri(); ?>/dist/img/chodakowska_thumb.png">
                <iframe class="left" src="https://www.youtube.com/embed/bHNtm4d_v6U?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Trening z Ewą Chodakowską</span>
                </div>
            </div>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="<?php echo get_template_directory_uri(); ?>/dist/img/teatr_thumb.png">
                <iframe class="right" src="https://www.youtube.com/embed/SXt4eVGDnzE?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Premiera opery "Poławiacze Pereł" <br> i kolacja w hotelu Sofitel Grand Sopot</span>
                </div>
            </div>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="<?php echo get_template_directory_uri(); ?>/dist/img/sarsa_thumb.png">
                <iframe src="https://www.youtube.com/embed/PdmsSC4W864?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Kameralny koncert Sarsy w hotelu ibis Warszawa Centrum</span>
                </div>
            </div>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="<?php echo get_template_directory_uri(); ?>/dist/img/castle_thumb.png">
                <iframe src="https://www.youtube.com/embed/FQESs3pbUBg?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Kolacja na Zamku Królewskim w Warszawie i koncert Grupy MoCarta</span>
                </div>
            </div>
        </div>
        
    </section>
<?php
  get_footer();
?>