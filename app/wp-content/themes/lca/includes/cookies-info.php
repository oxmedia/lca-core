<div class="cookies-info" id="cookies">
    <h3>Informacja o ciasteczkach</h3>
    <span>W celu świadczenia usług na najwyższym poziomie strona wykorzystuje pliki cookie. Korzystając ze strony wyrażasz zgodę na zbieranie anonimowych danych, które pomagają nam w dostosowaniu treści do Twoich zainteresować (personalizacja plików cookie) oraz przedstawiając odpowiednie dla Ciebie informację i reklamy (pliki cookie do targetowania).</span>
    <a id="acceptCookies" class="button">Rozumiem</a>
</div>