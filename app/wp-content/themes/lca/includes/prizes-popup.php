
<!-- prizes-popup -->
<div class="page-home__slider__slide__popup-wrapper" id="homePopup">
              <div class="page-home__slider__slide__popup-background" id="homePopup_bckg"></div>
              <div class="page-home__slider__slide__box-wrapper page-home__slider__slide__box-wrapper--popup">
                <div class="page-home__slider__slide__box page-home__slider__slide__box--popup" id="homePopup_box">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/home-gift.png" class="page-home__slider__slide__box-gift-img" id="homePopup_gift">
                  <div class="page-home__slider__slide__box-top page-home__slider__slide__box-top--popup">
                    <div class="page-home__slider__slide__box-header">
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--light
                                   page-home__slider__slide__box-header-font--smll-line-height">Wygraj </span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--bold
                                   page-home__slider__slide__box-header-font--smll-line-height">50 000, 20 000 </span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--light
                                   page-home__slider__slide__box-header-font--smll-line-height">lub </span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--bold
                                   page-home__slider__slide__box-header-font--smll-line-height">10 000</span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--light
                                   page-home__slider__slide__box-header-font--smll-line-height">punktów Rewards i wymieniaj je na:</span>
                    </div>
                    <div class="page-home__slider__slide__box-products-col">
                      <div class="page-home__slider__slide__box-product-row">
                        <div class="page-home__slider__slide__box-product-row__image">
                          <img src="<?php echo get_template_directory_uri(); ?>/img/LaCollection_facebook_1200x900.jpg">
                        </div>
                        <div class="page-home__slider__slide__box-product-row__name">
                          <a>Produkty z La Collection</a>
                        </div>
                      </div>
                      <div class="page-home__slider__slide__box-product-row">
                        <div class="page-home__slider__slide__box-product-row__image">
                          <img src="<?php echo get_template_directory_uri(); ?>/img/Elite Experience 1.jpg">
                        </div>
                        <div class="page-home__slider__slide__box-product-row__name">
                          <a>Wydarzenia VIP tylko dla klubowiczów</a>
                        </div>
                      </div>
                      <div class="page-home__slider__slide__box-product-row page-home__slider__slide__box-product-row--last">
                        <div class="page-home__slider__slide__box-product-row__image">
                          <img src="<?php echo get_template_directory_uri(); ?>/img/Fairmont Maldives - Sirru Fen Fushi - Gaakoshinbi.jpg">
                        </div>
                        <div class="page-home__slider__slide__box-product-row__name">
                          <a>Podróż marzeń</a>
                        </div>
                      </div>
                    </div>
                    <div class="page-home__slider__slide__box-buttons-wrapper
                                page-home__slider__slide__box-buttons-wrapper--popup">
                      <a href="<?php echo get_home_url(); ?>/konkurs#formSection" class="yellow">Weź udział w konkursie</a>
                    </div>
                  </div>
                  <div class="page-home__slider__slide__box-bottom page-home__slider__slide__box-bottom--popup">
                    <a class="page-home__slider__slide__box-close-popup-btn" id="homePopup_switchOffBtn">Powrót</a>
                  </div>
                </div>
              </div>
            </div>

<!-- END prizes-popup -->