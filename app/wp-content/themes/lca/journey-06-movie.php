<?php /* Template Name: Podróż 06 - film */ ?>
<?php
  get_header();
?>
    <section class="page-movie">
    <?php include('includes/prizes-popup.php'); ?>
        <div class="page-movie__movie">
            <video src="<?php echo get_template_directory_uri(); ?>/videos/AdobeStock_184178374_x264.mp4" playsinline autoplay muted loop></video>
        </div>
        <div class="page-movie__wrapper">
            <div class="page-movie__wrapper-top">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll-white.png">
                <span class="page-movie__heading page-movie__heading--white">Bez względu na to, które wydarzenie <a href="https://www.accorhotels.com/pl/leclub/use/elite-experiences.shtml" target="_blank">Elite Experience</a> wybierzesz lub w jakim hotelu z kartą Le Club AccorHotels się zatrzymasz będziesz w centrum naszej uwagi.</span>
            </div>
            <div class="page-movie__wrapper-bottom">
                <a target="_blank" href="https://www.accorhotels.com/leclub/polska/join-loyalty-program/index.pl.shtml" class="page-movie__button page-movie__button--yellow">Zarezerwuj podróż z Le Club AccorHotels</a>
                <a class="page-movie__button page-movie__button--yellow" href="<?php echo get_home_url(); ?>/konkurs">Weź udział w konkursie</a>
            </div>
        </div>
    </section>
<?php
  get_footer();
?>