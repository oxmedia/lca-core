/*-- OVERALL --*/

var windowWidth = $(window).width();

//-- top navigation
var $navbar = $("#navbar");
var $mobileCollapse = $("#mobileCollapse");

$mobileCollapse.on("click", function(){
  $navbar.toggleClass('navbar__mobile-collapsed')
})

function redirectOnMobile(){
  windowWidth = $(window).width();
  if (windowWidth < 992) {
    var url = document.location.href;
    var res = url.split("/");
    res[res.length - 2] = "wiecej-niz-10";
    var url = res.join("/");
    window.location.href = url;  
  } 
} 

if( $(".page_entrance").length ||
    $(".page-lobby").length || 
    $(".page-hotelRoom").length ||
    $(".page-laptopRoom").length ||
    $(".page-events").length ||
    $(".page-movie").length 
  ){
    $("#navAdvantages").addClass("current_page_item");
}
  
/*-- HOME PAGE --*/   

//-- home carousel
// var $homeCarousel = $("#homeCarousel");

// $homeCarousel.owlCarousel({
//     loop:false,
//     mouseDrag:false,
//     autoplay:true,
//     autoplayTimeout:15000, 
//     margin:0,
//     nav:true,
//     responsive:{
//         0:{  
//             items:1
//         }
//     } 
// });
//  /* keyboard nav */
//  $(document).on('keydown', function( event ) { //attach event listener
//     if(event.keyCode == 37) {
//         $homeCarousel.trigger('prev.owl')
//     }
//     if(event.keyCode == 39) {
//         $homeCarousel.trigger('next.owl')
//     }
// }); 
// /* scroll nav */
// $(window).on('mousewheel', function(e){
//   if (e.originalEvent.deltaY < 0) {
//     $homeCarousel.trigger('next.owl')
//   }
//   if (e.originalEvent.deltaY > 0) {
//     $homeCarousel.trigger('prev.owl')
//   }
// })

//-- home toggle popup
var $homePopup = $("#homePopup");
var $homePopup_bckg = $("#homePopup_bckg");
var $homePopup_box = $("#homePopup_box");
var $homePopup_gift = $("#homePopup_gift");
var $homePopup_switchOnBtn = $("#homePopup_switchOnBtn");
var $homePopup_switchOffBtn = $("#homePopup_switchOffBtn");
var $homeBox = $("#homeBox");

//params: 1)string: define whether we want to show or hide popup
function toggleHomePopup(action){ 
  if (action === "on") { 
    $homePopup.addClass("active");
    $homeBox.removeClass("active");
    setTimeout(function(){
      $homePopup_box.addClass("active"); 
      $homePopup_gift.addClass("active"); 
    }, 5);
    if(windowWidth < 993 && $("#navbar").hasClass("navbar__mobile-collapsed")){
      $(".navbar-toggler").click();
    }
  }else if(action === "off"){
    $homePopup_box.removeClass("active");
    $homePopup_gift.removeClass("active");
    $homeBox.addClass("active");
    setTimeout(function(){
      $homePopup.removeClass("active"); 
    }, 330);
  }
}

//bind toggling function to DOM elements
$homePopup_switchOnBtn.on("click", function(){
  toggleHomePopup("on");
});

$(".popup-menu").on("click", function(){
  toggleHomePopup("on");
  $(".popup-menu").addClass("current_page_item");
}) 

$homePopup_bckg.on("click", function(){
  toggleHomePopup("off");
  $(".popup-menu").removeClass("current_page_item");
})
$homePopup_switchOffBtn.on("click", function(){
  toggleHomePopup("off");
  $(".popup-menu").removeClass("current_page_item");
})
$(document).keyup(function(e) {
     if (e.keyCode == 27) {
       toggleHomePopup("off");
       $(".popup-menu").removeClass("current_page_item");
    } 
});

/*-- TRAVEL PAGE --*/
//-- toggle hotel gallery box
//show
$(".gallery-box-wrapper").on("click", function(e){
  var $galleryPopup = $(this).find(".page-travel__hotel-preview-box-opened");
  if (!$galleryPopup.hasClass("active") && !$(e.target).hasClass("bckg") && !$(e.target).hasClass("close-box") && !$(e.target).hasClass("arrows-container") && !$(e.target).hasClass("gallery-arrow-left") && !$(e.target).hasClass("gallery-arrow-right")) {
    $galleryPopup.addClass("active");
  }
})
//hide
$(".gallery-box-wrapper").find(".bckg").on("click", function(){
  $(this).parent().removeClass("active");
})
$(".gallery-box-wrapper").find(".close-box").on("click", function(){
  $(this).parent().parent().removeClass("active");
})

$(".gallery-arrow-right").on("click", function(){
  galleryNext($(this).parent().parent().parent())
})
$(".gallery-arrow-left").on("click", function(){
  galleryPrev($(this).parent().parent().parent())
})

function galleryNext(imageWrapper){
  imageWrapper.parent().find(".gallery-box-wrapper").each(function(){
    $(this).find(".page-travel__hotel-preview-box-opened").removeClass("active");
  })
  if (imageWrapper.next().length != 0) {
    imageWrapper.next().find(".page-travel__hotel-preview-box-opened").addClass('active');
  }else{
    imageWrapper.parent().find(".gallery-box-wrapper").eq(0).find(".page-travel__hotel-preview-box-opened").addClass('active');
  }
}
function galleryPrev(imageWrapper){
  imageWrapper.parent().find(".gallery-box-wrapper").each(function(){
    $(this).find(".page-travel__hotel-preview-box-opened").removeClass("active");
  })
  if (imageWrapper.prev().length != 0) {
    imageWrapper.prev().find(".page-travel__hotel-preview-box-opened").addClass('active');
  }else{
    imageWrapper.parent().find(".gallery-box-wrapper").last().find(".page-travel__hotel-preview-box-opened").addClass('active');
  }
}

//-- send image form
var $formChooseImage = $("#formChooseImage");
var $formImageButton = $("#formImageButton");
var $formPreviewImage = $("#formPreviewImage");
 
//choose image
$formChooseImage.on("change", function(e){
  var file = e.target.files[0];
  var reader = new FileReader();
  reader.onloadend = function() { 
    $formPreviewImage.attr("src", reader.result);
    $formImageButton.text("Wybierz inne zdjęcie");
  }
  reader.readAsDataURL(file);  
}) 

//track successful posts
if($("#postSend").length){
  console.log("pixel event on successful image send.");
  fbq('track', 'Purchase'); 
}


//-- toggle gallery box user images
//show
$(".contest-photo-box").on("click", function(e){
  var $opened = $(this).find('.page-travel__photos-photo-box-opened');
  if (!$opened.hasClass("active") && !$(e.target).hasClass("page-travel__photos-photo-box-opened-bckg") && !$(e.target).hasClass("page-travel__photos-photo-box-opened-close")) {

    $opened.addClass("active")
  }
})
//hide
$(".page-travel__photos-photo-box-opened-close").on("click", function(){
  var container = $(this).parent().parent();
  if (container.hasClass('active')) {
    $(container).removeClass("active");
  }
})
$(".page-travel__photos-photo-box-opened-bckg").on("click", function(){
  var container = $(this).parent();
  if (container.hasClass('active')) {
    $(container).removeClass("active");
  }
})


/*-- JOURNEY ENTRANCE PAGE --*/
var $hotelDoors = $("#hotelDoors");
var $hotelsSlideshow = $("#hotelsSlideshow");
var $hotelLightsOn = $("#hotelLightsOn");
var $benefitsCarousel = $("#benefitsCarousel");
var $videoAnimation = $("#videoAnimation");

$hotelsSlideshow.owlCarousel({
  responsive : {
    380:{
      items:1
    },
    480:{
      items:2
    },
    768:{
      items:3
    },
    1024:{
      items:5
    } 
  },
  dots: false,
  autoplay: true,
  autoplayTimeout: 2000,
  autoplaySpeed: 2000,
  loop: true,
  margin: 40,
  smartSpeed:450,
  fluidSpeed: 400, 
  slideTransition: "linear"
})

$benefitsCarousel.owlCarousel ({
  responsive : {
    0:{
      items:1
    }
  },  
  dots: false,
  onTranslated: function(){ 
    if($('#benefitsCarousel .owl-stage-outer .owl-stage .owl-item').last().hasClass('active')){
      $('.owl-next').css("opacity", 0);
      $('.owl-prev').css("opacity", 1);
    }else{
      $('.owl-next').show();
    }
    if($('#benefitsCarousel .owl-stage-outer .owl-stage .owl-item').first().hasClass('active')){
        $('.owl-prev').css("opacity", 0);
        $('.owl-next').css("opacity", 1);
    }else{
      $('.owl-prev').css("opacity", 1);
    }
  }
}) 

$hotelDoors.on("click", function(e){
  e.preventDefault();
  var url = $(this).attr("href");
  $videoAnimation.css("opacity", "1");
  $videoAnimation.find("video")[0].play();
  setTimeout(function(){ 
    //next page 
    window.location.href = url; 
  }, 500) 
})


/*-- JOURNEY LOBBY PAGE --*/
if ($(".page-lobby").length) {
  var $nextBtn = $(".next");
  var $videoAnimation = $("#videoAnimation"); 
 
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();  
  });

  $nextBtn.on("click", function(e){
    e.preventDefault();
    var url = $(this).attr("href");  
    $videoAnimation.css("opacity", "1");
    $videoAnimation.find("video")[0].play(); 
    $(".page-lobby__content").remove(); 
    setTimeout(function(){ 
      //next page 
      window.location.href = url; 
    }, 500)   
  });
} 
 
/*-- JOURNEY ROOM PAGE --*/
if ($(".page-hotelRoom").length) {
  var $nextBtn = $(".next");
  var $animation = $("#videoAnimation"); 
  var $backgroundVideo = $("#backgroundVideo");

  redirectOnMobile();
  $(window).on("resize", function(){ 
    redirectOnMobile();
  });

  $backgroundVideo[0].play();

  $nextBtn.on("click", function(e){
    e.preventDefault();
    var url = $(this).attr("href");  
    $videoAnimation.css("opacity", "1");
    $videoAnimation.find("video")[0].play();  
    setTimeout(function(){ 
      //next page 
      window.location.href = url; 
    }, 500)   
  });
}

/*-- JOURNEY LAPTOP ROOM PAGE --*/
if ($(".page-laptopRoom").length) {
  var $msgNotification = $("#msgNotification");
  var $infoBox = $("#infoBox"); 
  var $msgIcon = $("#msgIcon");
  var $closeBoxButton = $("#closeBoxButton");
  var $iframeSite = $("#iframeSite");
  var $videoIntro = $("#videoIntro");

  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile(); 
  });

  setTimeout(function(){
    $videoIntro.css({
      "opacity": "0",
      "pointerEvents": "none"
    })
  }, 500);

  setTimeout(function(){
    $msgNotification.addClass("active");
    setTimeout(function(){
      $msgNotification.removeClass("active");
      $msgIcon.addClass("active");
    }, 2000)
  }, 3750)
 
  $msgIcon.on("click", function(){
    $infoBox.addClass('active');
  }) 
 
  $closeBoxButton.on("click", function(){
    $infoBox.removeClass('active');
  })
  
}

/*-- JOURNEY EVENTS PAGE --*/
if ($(".page-events").length) {
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });
  
  $(".page-events__videos-video").on("click", function(){
    var $video = $(this).find("iframe");

    $(this).find(".page-events__videos-video-thumb, .page-events__videos-video-content").remove(); 
    $video[0].src += "&autoplay=1";
  })  
}  

/*-- JOURNEY VIDEO PAGE --*/ 
if ($(".page-movie").length) {
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });
}
 
/*-- SINGLE POST PAGE --*/
var $likes = $("#likes");
var $likesBtn = $(".vortex-container-vote");

var $sharePost = $("#sharePost");
 
$likesBtn.on("click", function(){
  var actualLikes = $(this).find(".vortex-p-like span").text();
  setTimeout(function(){ 
    $likes.text(actualLikes);
  }, 2000)
}) 
 
$sharePost.on("click", function(){
  elem = $(this);
  postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));

  return false;
}) 

 
/*-- COOKIES BOX --*/
var $cookies = $("#cookies");
var $cookiesBtn = $("#acceptCookies");

if(localStorage.getItem('cookies') == null){
  $cookies.addClass("active"); 
}
 
$cookiesBtn.on("click", function(){
  $cookies.removeClass("active");
  localStorage.setItem('cookies', true); 
})
