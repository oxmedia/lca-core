<?php
define("POSTS_PER_PAGE", 30);

//include scripts and styles
function theme_name_scripts() {
    wp_enqueue_style( 'style-css', get_template_directory_uri().'/css/main.min.css');
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.js',array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

add_theme_support( 'post-thumbnails' );

//custom menu
function register_custom_menu() {
	register_nav_menus(
		array(
			'main_menu' => __( 'Main Menu' )
		)
	);
}

add_action('init', 'register_custom_menu');
 
//set cookie
function set_unique_cookie(){
    
    if( !isset($_COOKIE['unique_user']) ){
        $value = generateRandomString();
        setcookie('unique_user', $value, time() + (31 * 24 * 3600), COOKIEPATH, COOKIE_DOMAIN);
    }
    
}

add_action('init', 'set_unique_cookie');

function generateRandomString($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

//create post types
function create_post_type() {
	register_post_type( 'zgloszenia',
	  array(
		'label'               => __( 'zgloszenia' ),
        'description'         => __( 'Zgłoszenia' ),
        'labels'              => array(
			'name'                => _x( 'Zgłoszenia', 'Post Type General Name' ),
			'singular_name'       => _x( 'Zgłoszenie', 'Post Type Singular Name' ),
			'menu_name'           => __( 'Zgłoszenia' ),
			'all_items'           => __( 'Wszystkie zgłoszenia' ),
			'view_item'           => __( 'Zobacz zgłoszenie' ),
			'add_new_item'        => __( 'Dodaj zgłoszenie' ),
			'add_new'             => __( 'Dodaj nowe zgłoszenie' ),
			'edit_item'           => __( 'Edytuj zgłoszenie' ),
			'update_item'         => __( 'Aktualizuj zgłoszenie' ),
			'search_items'        => __( 'Szukaj zgłoszeń' ),
			'not_found'           => __( 'Nie znaleziono' ),
			'not_found_in_trash'  => __( 'Nie znaleziono w koszu' ),
		),
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'show_in_rest'       => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
	  )
	);
}
  add_action( 'init', 'create_post_type' );

function get_following_post_id(){
	
	global $wpdb;

	$myrows = $wpdb->get_results( "SELECT ID FROM ".$wpdb->base_prefix."posts ORDER BY id DESC LIMIT 1" );
	$id = $myrows[0]->ID;

	return $id + 1;

}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}

//check whether user with this unique id already submited
function user_duplicate(){

    global $wpdb;

	$rowCount = $wpdb->get_var( "
        SELECT post_id 
        FROM ".$wpdb->base_prefix."postmeta 
        WHERE meta_key='unique_user' AND meta_value='".$_COOKIE['unique_user']."'
        ");
    
    if( is_null($rowCount) ){
        return false;
    }else{
        return true;
    }

}

//check if contest has started
function contest_started(){
    if( date("Y-m-d h:i:s") > "2018-08-23 00:00:00" && 
        date("Y-m-d h:i:s") < "2018-08-28 00:00:01"){
        return true;
    }else{
        return false;
    }
}