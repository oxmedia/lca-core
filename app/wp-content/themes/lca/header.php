<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php bloginfo( 'name' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta 
      property="og:image"   content="https://wiecejniz10.pl/wp-content/uploads/2018/08/lca-prev.png" />
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '982225925287477'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=982225925287477&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.1&appId=1231310603678070&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    function postToFeed(title, desc, url, image){
    var obj = {method: 'feed',link: url, picture: 'http://www.url.com/images/'+image,name: title,description: desc};
    function callback(response){}
    FB.ui(obj, callback);
    }
    </script>
    <?php wp_head();?>
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <a class="logo" href="<?php echo get_home_url(); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo.png" alt="">
    </a>
    <!-- navigation -->
    
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/hamburger.png">
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="list-wrapper mr-auto">
          <?php wp_nav_menu(array(
            'menu_class' => 'menu',
            'container' => false
          )); ?>  
          <?php wp_nav_menu(array(
            'menu_class' => 'menu menu--padding-left',
            'container' => false,
            'menu' => 'menu2'
          )); ?>
        </div>
        <div class="navbar__social">
          <a class="mobile" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/home-nav.png"></a>
          <a target="_blank" href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/social-fb.png"> </a>
          <a target="_blank" href="https://www.instagram.com/accorhotels_polska/"> <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
    <?php
    ?>