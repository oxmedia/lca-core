<?php /* Template Name: Konkurs */ ?>
<?php
  get_header();
?>
    <section class="page-travel">
    <?php include('includes/prizes-popup.php'); ?>
      <a href="#formSection" class="page-travel__contest-info">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll-white.png">
        <span class="page-travel__heading page-travel__heading--contest-info">Weź udział <br> w konkursie</span>
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/double-arrow.png">
      </a>
      <div class="page-travel__hero">
        <div class="page-travel__hero-top">
          <div class="page-travel__hero-top-wrapper">
              <span class="page-travel__heading page-travel__heading--big">Więcej niż </span>
              <span class="page-travel__heading page-travel__heading--big page-travel__heading--bold">10 pięknych widoków </span>
              <span class="page-travel__heading page-travel__heading--big">z okien naszych hoteli, </span>
              <span class="page-travel__heading">które możesz odwiedzić z Le Club AccorHotels</span>
          </div>
        </div>
        <div class="page-travel__hero-bottom">
          <div class="page-travel__hero-bottom-wrapper">
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ibis Berlin Mitte Germany.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-0357-ibis-berlin-mitte/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel ibis Berlin Mitte</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ibis Berlin Mitte Germany.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel ibis Berlin Mitte</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ibis Styles Ambassador Seoul Yongsan - South Korea.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Korea Południowa</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-9469-ibis-styles-ambassador-seoul-yongsan-seoul-dragon-city/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel ibis Styles Ambassador Seoul Yongsan - Seoul Dragon City</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ibis Styles Ambassador Seoul Yongsan - South Korea.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Korea Południowa</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel ibis Styles</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Mercure Liverpool Atlantic Tower Hotel UK.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Anglia</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-A0H9-mercure-liverpool-atlantic-tower-hotel/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Liverpool Atlantic Tower Hotel </a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Mercure Liverpool Atlantic Tower Hotel UK.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Anglia</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Liverpool Atlantic Tower Hotel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Mercure Courchevel - France.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">France</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/fr/hotel-0366-hotel-mercure-courchevel/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Mercure Courchevel</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Mercure Courchevel - France.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">France</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/PULLMAN LIVERPOOL.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Anglia</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-9227-pullman-liverpool/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Pullman Liverpool</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/PULLMAN LIVERPOOL.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Anglia</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Pullman Liverpool</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Sofitel Budapest chainbridge Hungary.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Węgry</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-3229-sofitel-budapest-chain-bridge/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Sofitel Budapest Chain Bridge</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Sofitel Budapest chainbridge Hungary.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Węgry</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Sofitel Budapest Chain Bridge</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Novotel Kraków Centrum.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Polska</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-3372-novotel-krakow-centrum/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Novotel Krakow Centrum</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Novotel Kraków Centrum.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Polska</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Novotel Krakow Centrum</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Sofitel Wrocław Old town.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Polska</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-5345-sofitel-wroclaw-old-town/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Sofitel Wroclaw Old Town</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Sofitel Wrocław Old town.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Polska</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Sofitel Wroclaw Old Town</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Mercure Tirrenia Green Park Calambrone Italy.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Włochy</span>
                    <div class="split"></div>
                    <a href="https://www.accorhotels.com/pl/hotel-B2S1-hotel-mercure-tirrenia-green-park-new-opening/index.shtml" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Mercure Tirrenia Green Park</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Mercure Tirrenia Green Park Calambrone Italy.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Włochy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Mercure Tirrenia Green Park</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Novotel RJ BOTAFOGO RIO DE JANEIRO BRAZIL.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Brazylia</span>
                    <div class="split"></div>
                    <a target="_blank" href="https://www.accorhotels.com/pl/hotel-9631-novotel-rj-botafogo/index.shtml?utm_term=nom&utm_campaign=ppc-nov-nom-goo-pl-pl-br-bro-sear-bp&utm_medium=cpc&utm_content=pl-pl-BR-V6871&utm_source=google" class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Novotel RJ Botafogo</a>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Novotel RJ BOTAFOGO RIO DE JANEIRO BRAZIL.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Brazylia</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Hotel Novotel RJ Botafogo</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="formSection" class="page-travel__signUp">
        <div class="page-travel__signUp-wrapper">
          <div class="page-travel__signUp-head">
            <h1 class="page-travel__heading page-travel__heading--big page-travel__heading--bold page-travel__heading--white">Zasady konkurs</h1>
            <div class="page-travel__signUp-subhead">
              <span class="page-travel__heading page-travel__heading--white">Pokaż nam własne, jedenaste miejsce, które odwiedziłeś z </span>
              <span class="page-travel__heading page-travel__heading--bold page-travel__heading--white">Le Club AccorHotels </span>
              <span class="page-travel__heading page-travel__heading--white">i zgłoś je do konkursu.</span>
              <br>
              <span class="page-travel__heading page-travel__heading--white">Wygraj:</span>
            </div>
          </div>
          <div class="page-travel__prizes-wrapper">
            <div class="page-travel__prize-box-mobile">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/nagrody-gif.gif">
            </div>
            <div class="page-travel__prize-box-wrapper">
              <div class="page-travel__prize-box">
                <div class="page-travel__prize-box-place">
                  <h2 class="page-travel__heading page-travel__heading--prize-place">I miejsce</h2>
                </div>
                <div class="page-travel__prize-box-prize">
                  <h1 class="page-travel__heading page-travel__heading--prize-points">50 000</h1>
                  <h3 class="page-travel__heading page-travel__heading--prize-subheader">punktów Rewards</h3>
                </div>
              </div>
            </div>
            <div class="page-travel__prize-box-wrapper">
              <div class="page-travel__prize-box">
                <div class="page-travel__prize-box-place">
                  <h2 class="page-travel__heading page-travel__heading--prize-place">II miejsce</h2>
                </div>
                <div class="page-travel__prize-box-prize">
                  <h1 class="page-travel__heading page-travel__heading--prize-points">20 000</h1>
                  <h3 class="page-travel__heading page-travel__heading--prize-subheader">punktów Rewards</h3>
                </div>
              </div>
            </div>
            <div class="page-travel__prize-box-wrapper">
              <div class="page-travel__prize-box">
                <div class="page-travel__prize-box-place">
                  <h2 class="page-travel__heading page-travel__heading--prize-place">III miejsce</h2>
                </div>
                <div class="page-travel__prize-box-prize">
                  <h1 class="page-travel__heading page-travel__heading--prize-points">10 000</h1>
                  <h3 class="page-travel__heading page-travel__heading--prize-subheader">punktów Rewards</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="page-travel__signUp-subheader">
            <span class="page-travel__heading page-travel__heading--white">za wygrane punkty wybierz się w </span>
            <a target="_blank" href="https://www.accorhotels.com/pl/leclub/use/booking-with-points.shtml" class="page-travel__heading page-travel__heading--underline page-travel__heading--white">podróż marzeń</a>
            <span class="page-travel__heading page-travel__heading--white">, zafunduj sobie </span>
            <a target="_blank" href="https://lacollectionbyleclubaccorhotels.com" class="page-travel__heading page-travel__heading--underline page-travel__heading--white">prezent z butiku La Collection</a>
            <span class="page-travel__heading page-travel__heading--white"> by Le Club AccorHotels lub jedno z </span>
            <a target="_blank" href="https://www.accorhotels.com/pl/leclub/use/elite-experiences.shtml" class="page-travel__heading page-travel__heading--underline page-travel__heading--white">wyjątkowych wydarzeń </a>
          </div>
          <div class="page-travel__signUp-split-border"></div>
          <div class="page-travel__signUp-how-to-wrapper">
            <h1 class="page-travel__heading page-travel__heading--big page-travel__heading--bold page-travel__heading--white">Jak wziąć udział w konkursie?</h1>
            <div class="page-travel__signUp-steps-wrapper">
              <div class="page-travel__signUp-step">
                <div class="page-travel__signUp-step-icon-wrapper">
                  <div class="page-travel__signUp-step-number-wrapper">
                    <div class="page-travel__signUp-step-number">
                      <span class="page-travel__heading page-travel__heading--step page-travel__heading--bolder">1</span>
                    </div>
                  </div>
                  <div class="page-travel__signUp-step-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/upload-image.png">
                  </div>
                  <div class="page-travel__signUp-step-icon-dots">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                  </div>
                </div>
                <div class="page-travel__signUp-step-content">
                  <span class="page-travel__heading page-travel__heading--step page-travel__heading--white">Prześlij zdjęcie</span>
                </div>
              </div>
              <div class="page-travel__signUp-step">
                <div class="page-travel__signUp-step-icon-wrapper">
                  <div class="page-travel__signUp-step-number-wrapper">
                    <div class="page-travel__signUp-step-number">
                      <span class="page-travel__heading page-travel__heading--step page-travel__heading--bolder">2</span>
                    </div>
                  </div>
                  <div class="page-travel__signUp-step-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/text-box.png">
                  </div>
                  <div class="page-travel__signUp-step-icon-dots">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                  </div>
                </div>
                <div class="page-travel__signUp-step-content">
                  <span class="page-travel__heading page-travel__heading--step page-travel__heading--white">Odpowiedz na pytanie konkursowe</span>
                </div>
              </div>
              <div class="page-travel__signUp-step">
                <div class="page-travel__signUp-step-icon-wrapper">
                  <div class="page-travel__signUp-step-number-wrapper">
                    <div class="page-travel__signUp-step-number">
                      <span class="page-travel__heading page-travel__heading--step page-travel__heading--bolder">3</span>
                    </div>
                  </div>
                  <div class="page-travel__signUp-step-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/voting.png">
                  </div>
                </div>
                <div class="page-travel__signUp-step-content">
                  <span class="page-travel__heading page-travel__heading--step page-travel__heading--white">Od 23.08 do 27.08 głosuj na najlepsze zgłoszenie</span>
                </div>
              </div>
            </div>
            <div class="page-travel__signUp-date">
              <span class="page-travel__heading page-travel__heading--white page-travel__steps">Czekamy na Wasze zgłoszenia </span>
              <span class="page-travel__heading page-travel__heading--white page-travel__heading--steps page-travel__heading--bolder">do 22 sierpnia!</span>
            </div>
          </div>
          <div class="page-travel__signUp-split-border"></div>
          <div class="page-travel__signUp-form-wrapper">
            <?php
              // get plugin *lca-konkurs* form
              echo do_shortcode('[lca_contest]');
            ?>
          </div>
        </div>
      </div>
      <div class="page-travel__photos">
        <div class="page-travel__photos-wrapper">
          <div class="page-travel__photos-header">
            <h1 class="page-travel__heading">Zwiedź więcej niż 10 miejsc z innymi klubowiczami i zobacz ich wspomnienia na zdjęciach!</h1>
          </div>
          <div class="page-travel__photos-gallery">
            <?php
                $loop = new WP_Query( array( 
                  'post_type' => 'zgloszenia', 
                  'post_status' => 'publish',
                  'posts_per_page' => 6,
                  'orderby' => 'rand'
                ) 
              );
            ?>
            <?php if( $loop->have_posts() ): ?>
              <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
              <?php 
                $urlLarge = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'large')[0];
              ?>
              <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
                <div class="page-travel__hotel-preview-box-opened">
                  <div class="arrows-container">
                    <div class="gallery-arrow-left">
                      <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                    </div>
                    <div class="gallery-arrow-right">
                      <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/keyboard-right-arrow-button.svg" alt="">
                    </div>
                  </div>
                  <div class="bckg"></div>
                  <div class="box">
                    <div class="box__close-btn close-box"></div>
                    <img src="<?php echo $urlLarge; ?>" alt="">
                  </div>
                </div>
                <div class="page-travel__hotel-preview-box">
                  <img src="<?php echo $urlLarge; ?>">
                  <div class="page-travel__hotel-preview-box-mask">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png" class="loupe">
                  </div>
                </div>
              </div>
              <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
          </div>
          <div class="page-travel__photos-button-wrapper">
            <a href="<?php echo get_home_url(); ?>/galeria-prac" class="page-travel__button page-travel__button--blue">Przejdź do galerii</a>
          </div>
        </div>
      </div>
    </section>
<?php
  get_footer("content");
?>