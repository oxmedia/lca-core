<?php /* Template Name: Podróż 03 - pokój hotelowy */ ?>
<?php
  get_header();
?>
        <section class="page-hotelRoom">
        <?php include('includes/prizes-popup.php'); ?>
            <!-- <div class="page-hotelRoom__background"></div> -->
            <div class="page-hotelRoom__video-bckg page-hotelRoom__video-bckg--start">
                <video id="backgroundVideo" src="<?php echo get_template_directory_uri(); ?>/videos/SCENA 3_2.mp4" playsinline autoplay muted loop></video>
            </div>
            <div id="videoAnimation" class="page-hotelRoom__video-bckg page-hotelRoom__video-bckg--animation">
                <video src="<?php echo get_template_directory_uri(); ?>/videos/SCENA 3_effect_1.mp4" mute></video>
            </div>
            <div class="page-hotelRoom__content">
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--gift">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-bottom">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Prezent powitalny</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Cieszymy się, że podróżujesz z Le Club AccorHotels, dlatego podczas każdego pobytu w naszych hotelach mamy przygotowany dla Ciebie prezent powitalny.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--standards">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-left">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Wyższy standard pokoju</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Nasi klubowicze zasługują na wszystko, co najlepsze. Dlatego z nami wypoczywasz w pokoju o wyższym standardzie.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--wifi">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-bottom">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Darmowe wi-fi</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Ciesz się z nami darmowym wi-fi i załatw więcej niż 10 spraw bez wychodzenia z pokoju hotelowego.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--checkin">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-right">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Późne wymeldowanie i wczesne zameldowanie na życzenie</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Przyjeżdżasz wcześnie rano? Wyjeżdżasz późno w nocy? Z Le Club AccorHotels zameldujesz się i wymeldujesz na życzenie - kiedy tylko chcesz.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--executiveLounge">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-bottom">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Dostęp do executive lounge</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Spędzisz więcej niż 10 minut w oczekiwaniu na samolot? Skorzystaj z executive lounge - z kartą LCAH masz dostęp do ponad 1000 prywatnych poczekalni na całym świecie.</span>
                    </div>
                </a>
                <a href="<?php echo get_home_url(); ?>/podroz-laptop" class="next page-hotelRoom__box page-hotelRoom__box--lacollection">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-right">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">La Collection</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Wejdź do świata La Collection i sprawdź na jakie produkty więcej niż 10 marek możesz wymienić punkty Rewards!</span>
                    </div>
                </a>
            </div>
            <div class="page-hotelRoom__arrow">
                <a href="<?php echo get_home_url(); ?>/podroz-lobby" class="page-lobby__arrow-wrapper page-lobby__arrow-wrapper--left">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow-right-bckg.png">
                    <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Do tyłu</span>
                </a>
                <a href="<?php echo get_home_url(); ?>/podroz-laptop" class="next page-hotelRoom__arrow-wrapper">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow-right-bckg.png">
                    <span class="page-hotelRoom__heading page-hotelRoom__heading--uppercase page-hotelRoom__heading--white page-hotelRoom__heading--arrow page-hotelRoom__heading--bolder page-hotelRoom__heading--upper">Przejdź dalej</span>
                </a>
            </div>
        </section>
<?php
  get_footer();
?>