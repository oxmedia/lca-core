<?php get_header( ); ?>
    <section class="page-home">
      <div class="page-home__slider" id="homeCarousel">
        <div class="page-home__slider__slide page-home__slider__slide--slide-1">
          <div class="page-home__slider__slide__content-wrapper">
            <div class="page-home__slider__slide__popup-wrapper" id="homePopup">
              <div class="page-home__slider__slide__popup-background" id="homePopup_bckg"></div>
              <div class="page-home__slider__slide__box-wrapper page-home__slider__slide__box-wrapper--popup">
                <div class="page-home__slider__slide__box page-home__slider__slide__box--popup" id="homePopup_box">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/home-gift.png" class="page-home__slider__slide__box-gift-img" id="homePopup_gift">
                  <div class="page-home__slider__slide__box-top page-home__slider__slide__box-top--popup">
                    <div class="page-home__slider__slide__box-header">
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--light
                                   page-home__slider__slide__box-header-font--smll-line-height">Wygraj </span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--bold
                                   page-home__slider__slide__box-header-font--smll-line-height">50 000, 20 000 </span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--light
                                   page-home__slider__slide__box-header-font--smll-line-height">lub </span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--bold
                                   page-home__slider__slide__box-header-font--smll-line-height">10 000</span>
                      <span class="page-home__slider__slide__box-header-font
                                   page-home__slider__slide__box-header-font--light
                                   page-home__slider__slide__box-header-font--smll-line-height">punktów Rewards i wymieniaj je na:</span>
                    </div>
                    <div class="page-home__slider__slide__box-products-col">
                      <div class="page-home__slider__slide__box-product-row">
                        <div class="page-home__slider__slide__box-product-row__image">
                          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/LaCollection_facebook_1200x900.jpg">
                        </div>
                        <div class="page-home__slider__slide__box-product-row__name">
                          <a>Produkty z La Collection</a>
                        </div>
                      </div>
                      <div class="page-home__slider__slide__box-product-row">
                        <div class="page-home__slider__slide__box-product-row__image">
                          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Elite Experience 1.jpg">
                        </div>
                        <div class="page-home__slider__slide__box-product-row__name">
                          <a>Wydarzenia VIP tylko dla klubowiczów</a>
                        </div>
                      </div>
                      <div class="page-home__slider__slide__box-product-row page-home__slider__slide__box-product-row--last">
                        <div class="page-home__slider__slide__box-product-row__image">
                          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/Fairmont Maldives - Sirru Fen Fushi - Gaakoshinbi.jpg">
                        </div>
                        <div class="page-home__slider__slide__box-product-row__name">
                          <a>Podróż marzeń</a>
                        </div>
                      </div>
                    </div>
                    <div class="page-home__slider__slide__box-buttons-wrapper
                                page-home__slider__slide__box-buttons-wrapper--popup">
                      <a href="<?php echo get_home_url(); ?>/konkurs#formSection" class="yellow">Weź udział w konkursie</a>
                    </div>
                  </div>
                  <div class="page-home__slider__slide__box-bottom page-home__slider__slide__box-bottom--popup">
                    <a class="page-home__slider__slide__box-close-popup-btn" id="homePopup_switchOffBtn">Powrót</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-home__slider__slide__box-wrapper">
              <div class="page-home__slider__slide__box active" id="homeBox">
                <div class="page-home__slider__slide__box-top">
                  <div class="page-home__slider__slide__box-header">
                    <span class="page-home__slider__slide__box-header-font">Poznaj</span>
                    <a href="<?php echo get_home_url(); ?>/wiecej-niz-10" class="page-home__slider__slide__box-header-font page-home__slider__slide__box-header-font--bold">więcej niż 10</a>
                    <span class="page-home__slider__slide__box-header-font">korzyści z programu</span><br>
                    <span class="page-home__slider__slide__box-header-font page-home__slider__slide__box-header-font--bold page-home__slider__slide__box-header-font--big">&nbsp;i weź udział w konkursie!</span>
                  </div>
                  <div class="page-home__slider__slide__box-list-wrapper">
                    <ul>
                      <li>Podziel się zdjęciem ze swojej podróży z Le Club AccorHotels,</li>
                      <li>weź udział w konkursie, wygraj punkty Rewards,</li>
                      <li>i zafunduj sobie wymarzoną wycieczkę lub niepowtarzalne produkty z La Collection.</li>
                    </ul>
                  </div>
                  <div class="page-home__slider__slide__box-buttons-wrapper">
                    <div class="page-home__slider__slide__box-buttons-wrapper--row">
                      <a href="#" class="yellow btn-50 upper" id="homePopup_switchOnBtn">Zobacz nagrody</a>
                      <a href="<?php echo get_home_url(); ?>/konkurs#formSection" class="yellow btn-50 upper">Weź udział w konkursie</a>
                    </div>
                    <a href="<?php echo get_home_url(); ?>/wiecej-niz-10">Poznaj więcej niż 10 korzyści</a>
                    
                  </div>
                </div>
                <div class="page-home__slider__slide__box-bottom">
                  <div class="page-home__slider__slide__box-images-row" href="#">
                    <?php
                      $loop = new WP_Query( array( 
                        'post_type' => 'zgloszenia', 
                         'post_status' => 'publish',
                         'posts_per_page' => 3,
                         'orderby' => 'rand'
                         ) 
                      );
                    ?>
                    <?php
                    
                    ?>
                    
                    <?php if( $loop->have_posts() ): ?>
                      <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
                      <?php 
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium')[0];
                      ?>
                        <a class="page-home__slider__slide__box-images-row-item" href="<?php the_permalink(); ?>">
                          <img src="<?php echo $url; ?>" alt="<?php echo get_the_title(); ?>">
                        </a>
                      <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                    <a href="<?php echo get_home_url(); ?>/konkurs" class="page-home__slider__slide__box-images-row-item
                                page-home__slider__slide__box-images-row-item--more-btn">
                      <span>+</span>
                    </a>
                  </div>
                  <a href="<?php echo get_home_url(); ?>/galeria-prac" class="page-home__slider__slide__box-see-photos-btn">Zobacz zdjęcia klubowiczów</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php get_footer( ); ?>