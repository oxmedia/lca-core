<?php /* Template Name: Galeria */ ?>
<?php get_header( ); ?>
    <section class="page-gallery">
    <?php include('includes/prizes-popup.php'); ?>
      <a href="<?php echo get_home_url(); ?>/wiecej-niz-10" class="page-travel__contest-info page-travel__contest-info--small">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll-white.png">
        <span class="page-travel__heading page-travel__heading--contest-info">Poznaj więcej<br>niż 10<br>korzyści programu</span>
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/double-arrow.png">
      </a>
      <div class="page-gallery__top">
        <div class="page-gallery__top-wrapper">
          <span class="page-gallery__header">Mamy dla Ciebie <span class="page-gallery__header--bolder">więcej niż 10</span> inspiracji na niezapomnianą podróż z Le Club AccorHotels. Zagłosuj na zdjęcie i opis, który najbardziej zainspirował Cię do kolejnej podróży!</span>
        </div>
      </div>
      <div class="page-gallery__gallery page-gallery__gallery--negative-top">
        <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $loop = new WP_Query( array( 
            'post_type'      => 'zgloszenia', 
            'post_status'    => 'publish',
            'posts_per_page' => POSTS_PER_PAGE,
            'order'          => 'DESC',
            'orderby'        => 'meta_value_num',
            'meta_key'       => 'vortex_system_likes',
            'paged'          => $paged
            ) 
          );
          $i = 0;
        ?>
        <?php if( $loop->have_posts() ): ?>
          <div class="page-gallery__gallery-wrapper">
            <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
              <?php 
                 $likes = get_post_meta(get_the_ID(),'vortex_system_likes',true);
                 $urlLarge = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'large')[0];
              ?>
              <a href="<?php echo get_permalink().'?postpage='.$paged; ?>" class="page-gallery__gallery-image">
                <div class="page-gallery__gallery-image-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
                </div>
                <?php if( $i < 20 && $paged == 1 ): ?>
                  <div class="page-gallery__gallery-image-like">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
                    <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box"><?php echo $likes; ?></span>
                  </div>
                <?php endif; ?>
                 
                <div class="page-gallery__gallery-image-box">
                  <img src="<?php echo $urlLarge; ?>">
                </div>
              </a>
              <?php $i++; ?>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <div class="page-gallery__pagination">
        <?php 
          echo paginate_links( array(
            'base' => str_replace( 3, '%#%', esc_url( get_pagenum_link( 3) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $loop->max_num_pages
          ) );
      ?>
      </div>
      <div class="page-gallery__bottom">
        <span class="page-gallery__header">Tak wyglądały Wasze podróże z Le Club AccorHotels! Dziękujemy za każdą z nich! Zobaczcie <span class="page-gallery__header--bolder">więcej niż 10</span> korzyści ze wspólnych podróży, które dla Was przygotowaliśmy!</span>
        <div class="page-gallery__bottom-button">
          <a class="page-gallery__button page-gallery__button--yellow" href="<?php echo get_home_url(); ?>/wiecej-niz-10">Poznaj więcej niż 10 korzyści</a>
        </div>
      </div>
    </section>

<?php
  get_footer("content");
?>
