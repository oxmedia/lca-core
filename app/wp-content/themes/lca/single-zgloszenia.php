<?php 
   /*
    * Template Name: Pojedyncze zgłoszenie
    * Template Post Type: zgloszenia
    */
    
?>
<?php get_header( ); ?>
    <?php 
      if( isset( $_GET['postpage'] ) ){
        $paged = $_GET['postpage'];
      }else{
        $paged = 1;
      }
    ?>
    <section class="page-gallery">
    <?php include('includes/prizes-popup.php'); ?>
      <a href="<?php echo get_home_url(); ?>/wiecej-niz-10" class="page-travel__contest-info page-travel__contest-info--small">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll-white.png">
        <span class="page-travel__heading page-travel__heading--contest-info">Poznaj więcej<br>niż 10<br>korzyści programu</span>
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/double-arrow.png">
      </a>
      <?php while ( have_posts() ) : the_post(); ?>
        <?php 
            $post_content = get_the_content();
            $likes = get_post_meta(get_the_ID(),'vortex_system_likes',true);
        ?>
        <div class="page-gallery__single-photo">
            <div class="page-gallery__single-photo-wrapper">
            <div class="page-gallery__single-photo-box">
                <div class="page-gallery__single-photo-box-image">
                <div class="page-gallery__single-photo-box-arrows">
                    <?php 
                        $pages = array();
                        $args = array(
                            'post_type'      => 'zgloszenia',
                            'post_status'    => 'publish',
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                            'orderby'        => 'meta_value_num',
                            'meta_key'       => 'vortex_system_likes'
                        );
                        $nav_posts = get_posts($args);
                        
                     
                        foreach($nav_posts as $nav_post) {
                            $pages[] += $nav_post->ID;
                       }
                        $id = get_the_id();
                     
                        $current = array_search($id, $pages);
                        $nextIndex = array_search($pages[$current+1], $pages);
                        $prevIndex = array_search($pages[$current-1], $pages);
                        $pagedPrev = ceil(($prevIndex + 1) / POSTS_PER_PAGE);
                        $pagedNext = ceil(($nextIndex + 1) / POSTS_PER_PAGE);

                        $prevID = $pages[$current-1];
                        $nextID = $pages[$current+1];

                        $prevURL = get_post_permalink( $prevID )."?postpage=".$pagedPrev;
                        $nextURL = get_post_permalink( $nextID )."?postpage=".$pagedNext;
                        $urlLarge = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'large')[0];

                        $pages = array();
                        $args = array(
                            'post_type'      => 'zgloszenia',
                            'post_status'    => 'publish',
                            'posts_per_page' => -1,
                            'order'          => 'DESC',
                           'orderby'        => 'meta_value_num',
                            'meta_key'       => 'vortex_system_likes'
                        );
                        $nav_posts = get_posts($args);
                        
                     
                        foreach($nav_posts as $nav_post) {
                            $pages[] += $nav_post->ID;
                       }
                        $id = get_the_id();
                     
                        $current = array_search($id, $pages);
                    ?>
                    
                    <a href="<?php echo $prevURL; ?>">
                        <?php if(!is_null($prevID)): ?> 
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow.png" class="page-gallery__single-photo-box-arrows-arrow page-gallery__single-photo-box-arrows-arrow-left" alt="">
                        <?php endif; ?>
                    </a>
                    <a href="<?php echo $nextURL; ?>">    
                        <?php if(!is_null($nextID)): ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow.png" class="page-gallery__single-photo-box-arrows-arrow 
                            page-gallery__single-photo-box-arrows-arrow-right" alt="">
                        <?php endif; ?>
                    </a>
                </div>
                <div class="page-gallery__single-photo-box-image-img" style="background-image: url(<?php echo $urlLarge; ?>)"></div>
                </div>
                <?php if( $current < 20 ): ?>
                <div class="page-gallery__single-photo-box-bottom">
                    <div class="page-gallery__single-photo-box-bottom-right">
                       <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png" class="page-gallery__single-photo-box-bottom-icon">
                        
                        <span class="page-gallery__header page-gallery__header--white page-gallery__header--single-photo page-gallery__header--bold"><span id="likes"><?php echo $likes; ?></span> lajków</span>
                    </div>
                  </div>
                <?php endif; ?>
            </div> 
            <div class="page-gallery__single-photo-share">
                <div class="page-gallery__single-photo-content page-gallery__header page-gallery__header--medium"><?php  echo $post_content;  ?></div>
                <div class="page-gallery__single-photo-share-button">
                  <a 
                      href="<?php echo get_permalink(); ?>"
                      id="sharePost" 
                      data-image="<?php echo get_the_post_thumbnail_url(); ?>" 
                      data-title="Le Club AccorHotels zdjęcie konkursowe"
                      data-desc="Le Club AccorHotels zdjęcie konkursowe"
                      class="page-gallery__button page-gallery__button--yellow">
                  Udostępnij</a>
                </div>
            </div>
            </div>
        </div>
      <?php endwhile; ?>
      <div class="page-gallery__gallery">
        <h1 class="page-gallery__gallery-heading page-gallery__header page-gallery__header--big">Zobacz inne zgłoszenia</h1>
        <?php
          $loop = new WP_Query( array( 
            'post_type'      => 'zgloszenia', 
            'post_status'    => 'publish',
            'posts_per_page' => POSTS_PER_PAGE,
            'orderby'        => 'meta_value_num',
            'order'          => 'DESC',
            'meta_key'       => 'vortex_system_likes',
            'paged'          => $paged
            ) 
          );
          $i = 0;
        ?>
        <?php if( $loop->have_posts() ): ?>
          <div class="page-gallery__gallery-wrapper">
            <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
              <?php 
                 $likes = get_post_meta(get_the_ID(),'vortex_system_likes',true);
                 $urlMid = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium')[0]; 
              ?>
              <a href="<?php echo get_permalink()."?postpage=".$paged; ?>" class="page-gallery__gallery-image">
                <div class="page-gallery__gallery-image-mask">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
                </div>
                <?php if($i < 20 && $paged == 1): ?>
                  <div class="page-gallery__gallery-image-like">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
                    <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box"><?php echo $likes; ?></span>
                  </div>
                <?php endif; ?>
                
                
                <div class="page-gallery__gallery-image-box">
                  <img src="<?php echo $urlMid; ?>">
                </div>
              </a>
              <?php $i++; ?>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <div class="page-gallery__bottom">
        <span class="page-gallery__header">Tak wyglądały Wasze podróże z Le Club AccorHotels! Dziękujemy za każdą z nich! Zobaczcie <span class="page-gallery__header--bolder">więcej niż 10</span> korzyści ze wspólnych podróży, które dla Was przygotowaliśmy!</span>
        <div class="page-gallery__bottom-button">
          <a class="page-gallery__button page-gallery__button--yellow" href="<?php echo get_home_url(); ?>/wiecej-niz-10">Poznaj więcej niż 10 korzyści</a>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__wrapper">
        <a href="index.php">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-transparent.png">
        </a>
        <a href="#" class="footer__ox-logo">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ox-logo.png">
        </a>
      </div>
    </footer>

    <?php get_footer( ); ?>

    <?php if( contest_started() ): ?>
      <script>
        $(".vortex-container-vote").css('display', 'flex');
      </script>
    <?php else: ?>
      <script>
        $(".vortex-container-vote").remove();
      </script>
    <?php endif; ?>