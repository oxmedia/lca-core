<?php /* Template Name: Podróż 04 - Laptop w pokoju */ ?>
<?php
  get_header();
?>
    <section class="page-laptopRoom">
    <?php include('includes/prizes-popup.php'); ?>
        <div class='page-laptopRoom__bckg'></div>
        <div id="videoIntro" class="page-laptopRoom__video-bckg page-laptopRoom__video-bckg--start">
          <video src="<?php echo get_template_directory_uri(); ?>/videos/SCENA 4_1.mp4" autoplay mute></video>
        </div>
        <div class='page-laptopRoom__site-container'>
            <div class='page-laptopRoom__site-wrapper'>
              <div class='page-laptopRoom__site-popup' id="msgNotification">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/mail.png" alt="">
                <span class="page-laptopRoom__heading page-laptopRoom__heading--white page-laptopRoom__heading--bolder page-laptopRoom__heading--mail-notification page-laptopRoom__heading--upper">Masz wiadomość</span>
              </div>
              <div class='page-laptopRoom__site-box' id="infoBox">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/open-mail.png" class="page-laptopRoom__site-box-mailIcon">
                <span class="page-laptopRoom__heading page-laptopRoom__heading--mid page-laptopRoom__heading--white page-laptopRoom__heading--thin">
                  Komfortowe hotele to dla nas za mało, a podróż z Le Club AccorHotels nie kończy się w ich progu. Dzięki wydarzeniom z cyklu <a class="page-laptopRoom__heading--bold">Elite Experiences</a> możesz przeżywać wyjątkowe chwile także poza naszymi hotelami!
                </span>
                <div class="page-laptopRoom__site-box-button">
                  <a class="page-laptopRoom__button page-laptopRoom__button--yellow" href="<?php echo get_home_url(); ?>/podroz-wydarzenia">Wybierz się z nami na jedno z nich!</a>
                </div>
                <img class='page-laptopRoom__site-box-closeButton' src='<?php echo get_template_directory_uri(); ?>/dist/img/icons/close-btn.png' id="closeBoxButton"></img>
              </div>
              <img class='page-laptopRoom__site-msgIcon' src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/mail-notification.gif" id="msgIcon"></img>
              <!-- <iframe id="iframeSite" class="page-laptopRoom__site-website" 
              src="https://lacollectionbyleclubaccorhotels.com/"></iframe> -->
              <video autoplay loop muted class="page-laptopRoom__site-website">
              <source src="<?php echo get_template_directory_uri(); ?>/videos/accor-wideo-finish.mp4" type="video/mp4">
              </video>
            </div>
        </div>
        <div class="page-laptopRoom__arrow">
            <a href="<?php echo get_home_url(); ?>/podroz-pokoj-hotelowy" class="page-lobby__arrow-wrapper page-lobby__arrow-wrapper--left">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow-right-bckg.png">
              <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Do tyłu</span>
            </a>
            <a href="<?php echo get_home_url(); ?>/podroz-wydarzenia" class="page-laptopRoom__arrow-wrapper">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow-right-bckg.png">
              <span class="page-laptopRoom__heading page-laptopRoom__heading--uppercase page-laptopRoom__heading--white page-laptopRoom__heading--arrow page-laptopRoom__heading--bolder page-laptopRoom__heading--upper">Przejdź dalej</span>
            </a>
        </div>
    </section>
<?php
  get_footer();
?>
