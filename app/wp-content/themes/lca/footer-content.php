    <?php include("includes/cookies-info.php"); ?>
    <?php 
      wp_footer(); 
    ?>
    <script>
        var siteURL = '<?php echo get_home_url(); ?>';
    </script>
    <footer class="footer">
      <div class="footer__wrapper">
        <a href="<?php echo get_home_url(); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-transparent.png">
        </a>
        <a href="https://oxmedia.pl" class="footer__ox-logo">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ox-logo.png">
        </a>
      </div>
    </footer>
</body>
</html>
