<?php /* Template Name: Galeria zdjęcie */ ?>
<?php get_header( ); ?>
    <section class="page-gallery">
    <?php include('includes/prizes-popup.php'); ?>
      <a href="<?php echo get_home_url(); ?>/wiecej-niz-10" class="page-travel__contest-info page-travel__contest-info--small">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll-white.png">
        <span class="page-travel__heading page-travel__heading--contest-info">Poznaj więcej<br>niż 10<br>korzyści programu</span>
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/double-arrow.png">
      </a>
      <div class="page-gallery__single-photo">
        <div class="page-gallery__single-photo-wrapper">
          <div class="page-gallery__single-photo-box">
            <div class="page-gallery__single-photo-box-image">
              <div class="page-gallery__single-photo-box-arrows">
                <a href="#">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow.png" class="page-gallery__single-photo-box-arrows-arrow page-gallery__single-photo-box-arrows-arrow-left" alt="">
                </a>
                <a href="#">
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow.png" class="page-gallery__single-photo-box-arrows-arrow page-gallery__single-photo-box-arrows-arrow-right" alt="">
                </a>
              </div>
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-single-example.png">
            </div>
            <div class="page-gallery__single-photo-box-bottom">
              <div class="page-gallery__single-photo-box-bottom-left">
                <div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-box-pin.png" class="page-gallery__single-photo-box-bottom-icon">
                  <span class="page-gallery__header page-gallery__header--white page-gallery__header--single-photo">Pullman Quay, Grand Sydney Harbour</span>
                </div>
                <div class="page-gallery__single-photo-box-bottom-split"></div>
                <div>
                  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png" class="page-gallery__single-photo-box-bottom-icon">
                  <span class="page-gallery__header page-gallery__header--white page-gallery__header--single-photo">Anna Kowalska</span>
                </div>
              </div>
              <div class="page-gallery__single-photo-box-bottom-right">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png" class="page-gallery__single-photo-box-bottom-icon">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--single-photo page-gallery__header--bold">&nbsp;15 lajków</span>
              </div>
            </div>
          </div>
          <div class="page-gallery__single-photo-share">
            <span class="page-gallery__header page-gallery__header--medium">Zatoka w Australii.</span>
            <div class="page-gallery__single-photo-share-button">
              <a class="page-gallery__button page-gallery__button--yellow">Udostępnij</a>
            </div>
          </div>
        </div>
      </div>
      <div class="page-gallery__gallery">
        <h1 class="page-gallery__gallery-heading page-gallery__header page-gallery__header--big">Zobacz inne zgłoszenia</h1>
        <div class="page-gallery__gallery-wrapper">
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-1.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-2.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-3.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-4.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-5.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-6.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-1.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-1.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-2.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-3.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-4.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-5.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-6.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/gallery-ex-1.png">
            </div>
          </a>
        </div>
      </div>
      <div class="page-gallery__bottom">
        <span class="page-gallery__header">Tak wyglądały Wasze podróże z Le Club AccorHotels! Dziękujemy za każdą z nich! Zobaczcie <span class="page-gallery__header--bolder">więcej niż 10</span> korzyści ze wspólnych podróży, które dla Was przygotowaliśmy!</span>
        <div class="page-gallery__bottom-button">
          <a class="page-gallery__button page-gallery__button--yellow" href="<?php echo get_home_url(); ?>/wiecej-niz-10">Poznaj więcej niż 10 korzyści</a>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__wrapper">
        <a href="index.php">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-transparent.png">
        </a>
        <a href="#" class="footer__ox-logo">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ox-logo.png">
        </a>
      </div>
    </footer>
    <?php get_footer( ); ?>