<?php /* Template Name: Podróż 02 - lobby */ ?>
<?php
  get_header();
?>

    <section class="page-lobby">
    <?php include('includes/prizes-popup.php'); ?>
      <!-- <div class="page-lobby__background"></div> -->
      <div class="page-lobby__video-bckg page-lobby__video-bckg--start">
        <video src="<?php echo get_template_directory_uri(); ?>/videos/SCENA_2_2.mp4" autoplay loop mute></video>
      </div>
      <div id="videoAnimation" class="page-lobby__video-bckg page-lobby__video-bckg--animation">
        <video src="<?php echo get_template_directory_uri(); ?>/videos/SCENA_2_effect_1.mp4" mute></video>
      </div>
      <div class="page-lobby__content">
        <a href="<?php echo get_home_url(); ?>/podroz-pokoj-hotelowy" class="next page-lobby__box page-lobby__box--card">
          <div class="page-lobby__box-copy page-lobby__box--arrow-right page-lobby__box--visible">
            <div class="page-lobby__box-copy-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Przejdź do swojego pokoju!</span>
          </div>
        </a>

        <a href="#" class="page-lobby__box page-lobby__box--terminal">
          <div class="page-lobby__box-copy page-lobby__box--arrow-bottom">
            <div class="page-lobby__box-copy-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Jako uczestnik Le Club AccorHotels korzystaj z ceny dla Klubowiczów i otrzymuj do 10% zniżki na swój pobyty!</span>
          </div>
        </a>

        <a href="#" class="page-lobby__box page-lobby__box--drink">
          <div class="page-lobby__box-copy page-lobby__box--arrow-bottom">
            <div class="page-lobby__box-copy-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Ciesz się wizytą w hotelu i rozpocznij swój pobyt z orzeźwiającym napojem w prezencie!</span>
          </div>
        </a>

        <a href="#" class="page-lobby__box page-lobby__box--keys">
          <div class="page-lobby__box-copy page-lobby__box--arrow-bottom">
            <div class="page-lobby__box-copy-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Z kartą LCAH zaoszczędzisz więcej niż 10 min na check-in! Zamelduj się on-line a w recepcji odbierz tylko klucze!</span>
          </div>
        </a>
      </div>
      <div class="page-lobby__arrow">
        <a href="<?php echo get_home_url(); ?>/wiecej-niz-10" class="page-lobby__arrow-wrapper page-lobby__arrow-wrapper--left">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow-right-bckg.png">
          <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Do tyłu</span>
        </a>
        <a href="<?php echo get_home_url(); ?>/podroz-pokoj-hotelowy" class="next page-lobby__arrow-wrapper">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/arrow-right-bckg.png">
          <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Przejdź dalej</span>
        </a>
      </div>
    </section>
<?php
  get_footer();
?>