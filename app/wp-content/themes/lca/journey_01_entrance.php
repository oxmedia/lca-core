<?php /* Template Name: Podróż 01 - wejście */ ?>
<?php
  get_header();
?>
    <section class="page_entrance">
    <?php include('includes/prizes-popup.php'); ?>
      <div class="page_entrance__entrance">
        <!-- <div class="page_entrance__entrance-bckg page_entrance__entrance-bckg--dark"></div>
        <div class="page_entrance__entrance-bckg page_entrance__entrance-bckg--light" id="hotelLightsOn"></div> -->
        <div class="page_entrance__entrance-bckg-video page_entrance__entrance-bckg-video--start">
          <video src="<?php echo get_template_directory_uri(); ?>/videos/SCENA_1_1.mp4" autoplay loop mute></video>
        </div>
        <div id="videoAnimation" class="page_entrance__entrance-bckg-video page_entrance__entrance-bckg-video--animation">
          <video src="<?php echo get_template_directory_uri(); ?>/videos/SCENA_1_effect_2.mp4" mute></video>
        </div>
        <div class="page_entrance__entrance-content">
          <a href="<?php echo get_home_url(); ?>/podroz-lobby" class="page_entrance__entrance-doors" id="hotelDoors">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/angled-arrow.png" alt="">
            <span>Wejdź do świata</span>
            <span>Le Club AccorHotels!</span>
          </a>
        </div>
      </div>
      <div class="page_entrance__entrance-static page_entrance__entrance-static--mobile">
      </div>
      <div class="page_entrance__entrance-benefits page_entrance__entrance-benefits--mobile">
        <div class="page_entrance__entrance-benefits-head">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
          <span class="page_entrance__heading">Poznaj <span class="page_entrance__heading page_entrance__heading--bolder">więcej niż 10 </span>korzyści</span>
          <span class="page_entrance__heading">Le Club AccorHotels</span>
        </div>
        <div class="page_entrance__entrance-benefits-carousel-wrapper">
          <div class="page_entrance__entrance-benefits-carousel" id="benefitsCarousel">

            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">01</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/hotel.png">
              </div>
              <h1 class="page_entrance__heading">Różne hotele</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Le Club AccorHotels to <span class="page_entrance__heading--medium">więcej niż 10</span> marek hoteli do wyboru</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">02</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/credit-card.png">
              </div>
              <h1 class="page_entrance__heading">Oszczędność czasu</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Z kartą Le Club AccorHotels zaoszczędzasz <span class="page_entrance__heading--medium">więcej niż 10</span> minut na check-in! Zamelduj się on-line, a w recepcji odbierz tylko klucze!</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">03</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/drink.png">
              </div>
              <h1 class="page_entrance__heading">Powitalny napój</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Ciesz się wizytą w hotelu i rozpocznij swój pobyt z orzeźwiającym drinkiem w prezencie!</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">04</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/credit-card-2.png">
              </div>
              <h1 class="page_entrance__heading">Cena dla klubowiczów</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Z kartą Le Club AccorHotels otrzymujesz nawet do 10% zniżki</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">05</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/gift.png">
              </div>
              <h1 class="page_entrance__heading">Oszczędność czasu</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Cieszymy się, że podróżujesz z Le Club AccorHotels, dlatego podczas każdego pobytu w naszych hotelach mamy przygotowany dla Ciebie prezent powitalny.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">06</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/wifi.png">
              </div>
              <h1 class="page_entrance__heading">Darmowe Wi-Fi</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Ciesz się z nami darmowym wi-fi zalatw <span class="page_entrance__heading--medium">więcej niż 10</span> spraw bez wychodzenia z pokoju hotelowego.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">07</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/bed.png">
              </div>
              <h1 class="page_entrance__heading">Wyższy komfort</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Nasi klubowicze zasługują na wszystko, co najlepsze. Dlatego z nami wypoczywasz w pokoju o wyższym standardzie.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">08</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/time.png">
              </div>
              <h1 class="page_entrance__heading">Więcej czasu</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Przyjeżdżasz wcześnie rano? Wyjeżdżasz późno w nocy? Z Le Club AccorHotels zameldujesz się i wymeldujesz na życzenie - kiedy tylko chcesz.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">09</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/prize.png">
              </div>
              <h1 class="page_entrance__heading">Produkty La Collection</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Wejdź do świata La Collection i wymieniaj punkty Rewards na produkty <span class="page_entrance__heading--medium">więcej niż 10</span> marek!</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">10</span>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/red-carpet.png">
              </div>
              <h1 class="page_entrance__heading">Elite Experience</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Więcej niż hotele! Dzięki Elite Experience masz dostęp do wyjątkowych koncertów, wydarzeń sportowych i kulturalnych w strefie vip.</span>
            </div>

          </div>
        </div>

      </div>
      <div class="page_entrance__hotels">
        <a href="#" class="page_entrance__box page_entrance__box--carousel page_entrance__box--carousel">
          <div class="page_entrance__box-copy page_entrance__box--arrow-bottom">
            <div class="page_entrance__box-copy-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/img/ah-logo-smll.png">
            </div>
            <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--copy-text">Le Club AccorHotels to więcej niż 10 marek hoteli do wyboru!</span>
          </div>
        </a>
        <div class="page_entrance__hotels-carousel" id="hotelsSlideshow">
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/gallery.png" alt="gallery">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/rixos.png" alt="rixos">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/sofitel.png" alt="sofitel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/fairmont.png" alt="fairmont">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/raffles.png" alt="raffles">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/novotel.png" alt="novotel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/sebel.png" alt="sebel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/grand-mercure.png" alt="grand-mercure">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/swissotel.png" alt="swissotel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/hotels/pullman.png" alt="pullman">
          </div>
        </div>
      </div>
    </section>
<?php
  get_footer();
?>