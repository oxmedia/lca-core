<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
    <section class="page-movie">
        <div class="page-movie__wrapper">
            <div class="page-movie__wrapper-top">
                <img src="img/ah-logo-smll-white.png">
                <span class="page-movie__heading page-movie__heading--white">...lub w jakim hotelu AccorHotels Zatrzymasz się, z kartą Le Club AccorHotels będziesz zawsze w centrum naszej uwagi.</span>
            </div>
            <div class="page-movie__wrapper-bottom">
                <a href="" class="page-movie__button page-movie__button--yellow">Zarezerwuj podróż z Le Club AccorHotels</a>
                <a class="page-movie__button page-movie__button--yellow" href="page_travel.php">Weź udział w konkursie</a>
            </div>
        </div>
    </section>
</body>
<script type="text/javascript" src="js/app.js"></script>
</html>