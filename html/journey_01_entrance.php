<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
    <section class="page_entrance">
      <div class="page_entrance__entrance">
        <div class="page_entrance__entrance-bckg page_entrance__entrance-bckg--dark"></div>
        <div class="page_entrance__entrance-bckg page_entrance__entrance-bckg--light" id="hotelLightsOn"></div>
        <div class="">
        </div>
        <div class="page_entrance__entrance-content">
          <a href="journey_02_lobby.php" class="page_entrance__entrance-doors" id="hotelDoors">
            <img src="img/icons/angled-arrow.png" alt="">
            <span>Wejdź do świata</span>
            <span>Le Club AccorHotels!</span>
          </a>
        </div>
      </div>
      <div class="page_entrance__entrance-static page_entrance__entrance-static--mobile">
      </div>
      <div class="page_entrance__entrance-benefits page_entrance__entrance-benefits--mobile">
        <div class="page_entrance__entrance-benefits-head">
          <img src="img/ah-logo-smll.png">
          <span class="page_entrance__heading">Poznaj <span class="page_entrance__heading page_entrance__heading--bolder">więcej niż 10 </span>korzyści</span>
          <span class="page_entrance__heading">Le Club AccorHotels</span>
        </div>
        <div class="page_entrance__entrance-benefits-carousel-wrapper">
          <div class="page_entrance__entrance-benefits-carousel" id="benefitsCarousel">

            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">01</span>
                </div>
                <img src="img/icons/hotel.png">
              </div>
              <h1 class="page_entrance__heading">Różne hotele</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Le Club AccorHotels to <span class="page_entrance__heading--medium">więcej niż 10</span> marek hoteli do wyboru</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">02</span>
                </div>
                <img src="img/icons/credit-card.png">
              </div>
              <h1 class="page_entrance__heading">Oszczędność czasu</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Z kartą Le Club AccorHotels zaoszczędzasz <span class="page_entrance__heading--medium">więcej niż 10</span> minut na check-in! Zamelduj się on-line, a w recepcji odbierz tylko klucze!</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">03</span>
                </div>
                <img src="img/icons/drink.png">
              </div>
              <h1 class="page_entrance__heading">Powitalny napój</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Ciesz się wizytą w hotelu i rozpocznij swój pobyt z orzeźwiającym drinkiem w prezencie!</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">04</span>
                </div>
                <img src="img/icons/credit-card-2.png">
              </div>
              <h1 class="page_entrance__heading">Cena dla klubowiczów</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Z kartą Le Club AccorHotels otrzymujesz nawet do 10% zniżki</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">05</span>
                </div>
                <img src="img/icons/gift.png">
              </div>
              <h1 class="page_entrance__heading">Oszczędność czasu</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Cieszymy się, że podróżujesz z Le Club AccorHotels, dlatego podczas każdego pobytu w naszych hotelach mamy przygotowany dla Ciebie prezent powitalny.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">06</span>
                </div>
                <img src="img/icons/wifi.png">
              </div>
              <h1 class="page_entrance__heading">Darmowe Wi-Fi</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Ciesz się z nami darmowym wi-fi zalatw <span class="page_entrance__heading--medium">więcej niż 10</span> spraw bez wychodzenia z pokoju hotelowego.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">07</span>
                </div>
                <img src="img/icons/bed.png">
              </div>
              <h1 class="page_entrance__heading">Wyższy komfort</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Nasi klubowicze zasługują na wszystko, co najlepsze. Dlatego z nami wypoczywasz w pokoju o wyższym standardzie.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">08</span>
                </div>
                <img src="img/icons/time.png">
              </div>
              <h1 class="page_entrance__heading">Więcej czasu</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Przyjeżdżasz wcześnie rano? Wyjeżdżasz późno w nocy? Z Le Club AccorHotels zameldujesz się i wymeldujesz na życzenie - kiedy tylko chcesz.</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">09</span>
                </div>
                <img src="img/icons/prize.png">
              </div>
              <h1 class="page_entrance__heading">Atrakcyjne nagrody</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Wejdź do świata La Collection i wymieniaj punkty Rewards na produkty <span class="page_entrance__heading--medium">więcej niż 10</span> marek!</span>
            </div>
            <div class="page_entrance__entrance-benefits-carousel-benefit">
              <div class="icon-wrapper">
                <div class="number">
                  <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--smaller page_entrance__heading--medium">10</span>
                </div>
                <img src="img/icons/red-carpet.png">
              </div>
              <h1 class="page_entrance__heading">Atrakcyjne nagrody</h1>
              <span class="page_entrance__heading page_entrance__heading--small">Więcej niż hotele! Dzięki Elite Experience masz dostęp do wyjątkowych koncertów, wydarzeń sportowych i kulturalnych w strefie vip.</span>
            </div>

          </div>
        </div>

      </div>
      <div class="page_entrance__hotels">
        <a href="#" class="page_entrance__box page_entrance__box--carousel page_entrance__box--carousel">
          <div class="page_entrance__box-copy page_entrance__box--arrow-bottom">
            <div class="page_entrance__box-copy-icon">
              <img src="img/ah-logo-smll.png">
            </div>
            <span class="page_entrance__heading page_entrance__heading--white page_entrance__heading--copy-text">Le Club AccorHotels to więcej niż 10 marek hoteli do wyboru!</span>
          </div>
        </a>
        <div class="page_entrance__hotels-carousel" id="hotelsSlideshow">
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/gallery.png" alt="gallery">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/rixos.png" alt="rixos">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/sofitel.png" alt="sofitel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/fairmont.png" alt="fairmont">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/raffles.png" alt="raffles">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/novotel.png" alt="novotel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/sebel.png" alt="sebel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/grand-mercure.png" alt="grand-mercure">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/swissotel.png" alt="swissotel">
          </div>
          <div class="page_entrance__hotels-carousel-item">
            <img src="img/hotels/pullman.png" alt="pullman">
          </div>
        </div>
      </div>
    </section>
  </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
