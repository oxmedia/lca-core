<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
    <section class="page-home">
      <div class="slider" id="homeCarousel">
        <div class="slider__slide slider__slide--slide-1">
          <div class="slider__slide__content-wrapper">
            <div class="slider__slide__popup-wrapper" id="homePopup">
              <div class="slider__slide__popup-background" id="homePopup_bckg"></div>
              <div class="slider__slide__box-wrapper slider__slide__box-wrapper--popup">
                <div class="slider__slide__box slider__slide__box--popup" id="homePopup_box">
                  <img src="img/home-gift.png" class="slider__slide__box-gift-img" id="homePopup_gift">
                  <div class="slider__slide__box-top slider__slide__box-top--popup">
                    <div class="slider__slide__box-header">
                      <span class="slider__slide__box-header-font
                                   slider__slide__box-header-font--light
                                   slider__slide__box-header-font--smll-line-height">Wygraj </span>
                      <span class="slider__slide__box-header-font
                                   slider__slide__box-header-font--bold
                                   slider__slide__box-header-font--smll-line-height">50 000, 20 000 </span>
                      <span class="slider__slide__box-header-font
                                   slider__slide__box-header-font--light
                                   slider__slide__box-header-font--smll-line-height">lub </span>
                      <span class="slider__slide__box-header-font
                                   slider__slide__box-header-font--bold
                                   slider__slide__box-header-font--smll-line-height">10 000</span>
                      <span class="slider__slide__box-header-font
                                   slider__slide__box-header-font--light
                                   slider__slide__box-header-font--smll-line-height">punktów Rewards i wymieniaj je na:</span>
                    </div>
                    <div class="slider__slide__box-products-col">
                      <div class="slider__slide__box-product-row">
                        <div class="slider__slide__box-product-row__image">
                          <img src="img/LaCollection_facebook_1200x900.jpg">
                        </div>
                        <div class="slider__slide__box-product-row__name">
                          <a>Produkty z La Collection</a>
                        </div>
                      </div>
                      <div class="slider__slide__box-product-row">
                        <div class="slider__slide__box-product-row__image">
                          <img src="img/Elite Experience 1.jpg">
                        </div>
                        <div class="slider__slide__box-product-row__name">
                          <a>Wydarzenia VIP tylko dla klubowiczów</a>
                        </div>
                      </div>
                      <div class="slider__slide__box-product-row slider__slide__box-product-row--last">
                        <div class="slider__slide__box-product-row__image">
                          <img src="img/Fairmont Maldives - Sirru Fen Fushi - Gaakoshinbi.jpg">
                        </div>
                        <div class="slider__slide__box-product-row__name">
                          <a>Podróż marzeń</a>
                        </div>
                      </div>
                    </div>
                    <div class="slider__slide__box-buttons-wrapper
                                slider__slide__box-buttons-wrapper--popup">
                      <a href="page_travel.php#formSection" class="yellow">Weź udział w konkursie</a>
                    </div>
                  </div>
                  <div class="slider__slide__box-bottom slider__slide__box-bottom--popup">
                    <a class="slider__slide__box-close-popup-btn" id="homePopup_switchOffBtn">Powrót</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="slider__slide__box-wrapper">
              <div class="slider__slide__box active" id="homeBox">
                <div class="slider__slide__box-top">
                  <div class="slider__slide__box-header">
                    <span class="slider__slide__box-header-font">Poznaj</span>
                    <span class="slider__slide__box-header-font slider__slide__box-header-font--bold">więcej niż 10</span>
                    <span class="slider__slide__box-header-font">korzyści z programu</span><br>
                    <span class="slider__slide__box-header-font slider__slide__box-header-font--bold slider__slide__box-header-font--big">&nbsp;i weź udział w konkursie!</span>
                  </div>
                  <div class="slider__slide__box-list-wrapper">
                    <ul>
                      <li>Podziel się zdjęciem ze swojej podróży z Le Club AccorHotels,</li>
                      <li>Weź udział w konkursie, wygraj punkty Rewards</li>
                      <li>I zafunduj sobie wymarzoną wycieczkę lub niepowtarzalne produkty z La Collection.</li>
                    </ul>
                  </div>
                  <div class="slider__slide__box-buttons-wrapper">
                    <a href="journey_01_entrance.php">Poznaj więcej niż 10 korzyści</a>
                    <div class="slider__slide__box-buttons-wrapper--row">
                      <a href="#" class="yellow btn-50" id="homePopup_switchOnBtn">Zobacz nagrody</a>
                      <a href="page_travel.php#formSection" class="yellow btn-50">Weź udział w konkursie</a>
                    </div>
                  </div>
                </div>
                <div class="slider__slide__box-bottom">
                  <div class="slider__slide__box-images-row" href="#">
                    <a href="gallery.php" class="slider__slide__box-images-row-item">
                      <img src="img/home-example-1.png" alt="">
                    </a>
                    <a href="gallery.php" class="slider__slide__box-images-row-item">
                      <img src="img/home-example-2.png" alt="">
                    </a>
                    <a href="gallery.php" class="slider__slide__box-images-row-item">
                      <img src="img/home-example-3.png" alt="">
                    </a>
                    <a href="page_travel.php" class="slider__slide__box-images-row-item
                                slider__slide__box-images-row-item--more-btn">
                      <span>+</span>
                    </a>
                  </div>
                  <a href="gallery.php" class="slider__slide__box-see-photos-btn">Zobacz zdjęcia klubowiczów i zagłosuj</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
