<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
    <section class="page-laptopRoom">
        <div class='page-laptopRoom__bckg'></div>
        <div class='page-laptopRoom__site-container'>
            <div class='page-laptopRoom__site-wrapper'>
              <div class='page-laptopRoom__site-popup' id="msgNotification">
                <img src="img/icons/mail.png" alt="">
                <span class="page-laptopRoom__heading page-laptopRoom__heading--white page-laptopRoom__heading--bolder page-laptopRoom__heading--mail-notification page-laptopRoom__heading--upper">Masz wiadomość</span>
              </div>
              <div class='page-laptopRoom__site-box' id="infoBox">
                <img src="img/icons/open-mail.png" class="page-laptopRoom__site-box-mailIcon">
                <span class="page-laptopRoom__heading page-laptopRoom__heading--mid page-laptopRoom__heading--white page-laptopRoom__heading--thin">
                  Komfortowe hotele to dla nas za mało, a podróż z Le Club AccorHotels nie kończy się w ich progu. Dzięki wydarzeniom z cyklu <span class="page-laptopRoom__heading--bold">Elite Experiences</span> możesz przeżywać wyjątkowe chwile także poza naszymi hotelami!
                </span>
                <div class="page-laptopRoom__site-box-button">
                  <a class="page-laptopRoom__button page-laptopRoom__button--yellow" href="journey-05-events.php">Wybierz się z nami na jedno z nich!</a>
                </div>
                <img class='page-laptopRoom__site-box-closeButton' src='img/icons/close-btn.png' id="closeBoxButton"></img>
              </div>
              <img class='page-laptopRoom__site-msgIcon' src="img/icons/mail-notification.gif" id="msgIcon"></img>
              <iframe id="iframeSite" class="page-laptopRoom__site-website" src="https://lacollectionbyleclubaccorhotels.com/"></iframe>
            </div>
        </div>
        <div class="page-laptopRoom__arrow">
            <a href="journey_03_hotelRoom.php" class="page-lobby__arrow-wrapper page-lobby__arrow-wrapper--left">
              <img src="img/icons/arrow-right-bckg.png">
              <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Do tyłu</span>
            </a>
            <a href="journey-05-events.php" class="page-laptopRoom__arrow-wrapper">
              <img src="img/icons/arrow-right-bckg.png">
              <span class="page-laptopRoom__heading page-laptopRoom__heading--uppercase page-laptopRoom__heading--white page-laptopRoom__heading--arrow page-laptopRoom__heading--bolder page-laptopRoom__heading--upper">Przejdź dalej</span>
            </a>
        </div>
    </section>
    </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
