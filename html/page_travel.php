<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
    <section class="page-travel">
      <a href="#formSection" class="page-travel__contest-info">
        <img src="img/ah-logo-smll-white.png">
        <span class="page-travel__heading page-travel__heading--contest-info">Weź udział <br> w konkursie</span>
        <img src="img/icons/double-arrow.png">
      </a>
      <div class="page-travel__hero">
        <div class="page-travel__hero-top">
          <div class="page-travel__hero-top-wrapper">
              <span class="page-travel__heading page-travel__heading--big">Więcej niż </span>
              <span class="page-travel__heading page-travel__heading--big page-travel__heading--bold">10 pięknych widoków </span>
              <span class="page-travel__heading page-travel__heading--big">z okien naszych hoteli, </span>
              <span class="page-travel__heading">które możesz odwiedzić z Le Club AccorHotels</span>
          </div>
        </div>
        <div class="page-travel__hero-bottom">
          <div class="page-travel__hero-bottom-wrapper">
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/ibis Berlin Mitte Germany.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/ibis Berlin Mitte Germany.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/ibis Styles Ambassador Seoul Yongsan - South Korea.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/ibis Styles Ambassador Seoul Yongsan - South Korea.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/Mercure Liverpool Atlantic Tower Hotel UK.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/Mercure Liverpool Atlantic Tower Hotel UK.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/Mercure Courchevel - France.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/Mercure Courchevel - France.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/PULLMAN LIVERPOOL.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/PULLMAN LIVERPOOL.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/Sofitel Budapest chainbridge Hungary.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/Sofitel Budapest chainbridge Hungary.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/Novotel Kraków Centrum.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/Novotel Kraków Centrum.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/The Sebel Brisbane.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/The Sebel Brisbane.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/Mercure Liverpool Atlantic Tower Hotel UK.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/Mercure Liverpool Atlantic Tower Hotel UK.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="page-travel__hero-hotel-wrapper gallery-box-wrapper">
              <div class="page-travel__hotel-preview-box-opened">
                <div class="arrows-container">
                  <div class="gallery-arrow-left">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                  <div class="gallery-arrow-right">
                    <img src="img/icons/keyboard-right-arrow-button.svg" alt="">
                  </div>
                </div>
                <div class="bckg"></div>
                <div class="box">
                  <div class="box__close-btn close-box"></div>
                  <img src="img/Novotel RJ BOTAFOGO RIO DE JANEIRO BRAZIL.jpg" alt="">
                  <div class="box__bottom-info">
                    <img src="img/icons/gallery-box-pin.png" class="pin">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
              <div class="page-travel__hotel-preview-box">
                <img src="img/Novotel RJ BOTAFOGO RIO DE JANEIRO BRAZIL.jpg">
                <div class="page-travel__hotel-preview-box-mask">
                  <img src="img/icons/gallery-loupe.png" class="loupe">
                  <div class="info-row">
                    <img src="img/icons/gallery-box-pin.png">
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Niemcy</span>
                    <div class="split"></div>
                    <span class="page-travel__heading page-travel__heading--white page-travel__heading--small">Mercure Courchevel</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="page-travel__signUp">
        <div class="page-travel__signUp-wrapper">
          <div class="page-travel__signUp-head">
            <h1 class="page-travel__heading page-travel__heading--big page-travel__heading--bold page-travel__heading--white">Zasady konkurs</h1>
            <div class="page-travel__signUp-subhead">
              <span class="page-travel__heading page-travel__heading--white">Pokaż nam własne, jedenaste miejsce, które odwiedziłeś z </span>
              <span class="page-travel__heading page-travel__heading--bold page-travel__heading--white">Le Club AccorHotels </span>
              <span class="page-travel__heading page-travel__heading--white">i zgłoś je do konkursu.</span>
              <br>
              <span class="page-travel__heading page-travel__heading--white">Wygraj:</span>
            </div>
          </div>
          <div class="page-travel__prizes-wrapper">
            <div class="page-travel__prize-box-wrapper">
              <div class="page-travel__prize-box">
                <div class="page-travel__prize-box-place">
                  <h2 class="page-travel__heading page-travel__heading--prize-place">I miejsce</h2>
                </div>
                <div class="page-travel__prize-box-prize">
                  <h1 class="page-travel__heading page-travel__heading--prize-points">50 000</h1>
                  <h3 class="page-travel__heading page-travel__heading--prize-subheader">punktów Rewards</h3>
                </div>
              </div>
            </div>
            <div class="page-travel__prize-box-wrapper">
              <div class="page-travel__prize-box">
                <div class="page-travel__prize-box-place">
                  <h2 class="page-travel__heading page-travel__heading--prize-place">II miejsce</h2>
                </div>
                <div class="page-travel__prize-box-prize">
                  <h1 class="page-travel__heading page-travel__heading--prize-points">20 000</h1>
                  <h3 class="page-travel__heading page-travel__heading--prize-subheader">punktów Rewards</h3>
                </div>
              </div>
            </div>
            <div class="page-travel__prize-box-wrapper">
              <div class="page-travel__prize-box">
                <div class="page-travel__prize-box-place">
                  <h2 class="page-travel__heading page-travel__heading--prize-place">III miejsce</h2>
                </div>
                <div class="page-travel__prize-box-prize">
                  <h1 class="page-travel__heading page-travel__heading--prize-points">10 000</h1>
                  <h3 class="page-travel__heading page-travel__heading--prize-subheader">punktów Rewards</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="page-travel__signUp-subheader">
            <span class="page-travel__heading page-travel__heading--white">i za wygrane punkty wybierz się w </span>
            <a href="https://www.accorhotels.com/pl/leclub/use/booking-with-points.shtml" class="page-travel__heading page-travel__heading--underline page-travel__heading--white">podróż marzeń</a>
            <span class="page-travel__heading page-travel__heading--white">, zafunduj sobie </span>
            <a href="https://lacollectionbyleclubaccorhotels.com" class="page-travel__heading page-travel__heading--underline page-travel__heading--white">prezent z butiku La Collection</a>
            <span class="page-travel__heading page-travel__heading--white"> by Le Club AccorHotels lub jedno z </span>
            <a href="https://www.accorhotels.com/pl/leclub/use/elite-experiences.shtml" class="page-travel__heading page-travel__heading--underline page-travel__heading--white">wyjątkowych wydarzeń </a>
          </div>
          <div class="page-travel__signUp-split-border"></div>
          <div class="page-travel__signUp-how-to-wrapper">
            <h1 class="page-travel__heading page-travel__heading--big page-travel__heading--bold page-travel__heading--white">Jak wziąć udział w konkursie?</h1>
            <div class="page-travel__signUp-steps-wrapper">
              <div class="page-travel__signUp-step">
                <div class="page-travel__signUp-step-icon-wrapper">
                  <div class="page-travel__signUp-step-number-wrapper">
                    <div class="page-travel__signUp-step-number">
                      <span class="page-travel__heading page-travel__heading--step page-travel__heading--bolder">1</span>
                    </div>
                  </div>
                  <div class="page-travel__signUp-step-icon">
                    <img src="img/icons/upload-image.png">
                  </div>
                  <div class="page-travel__signUp-step-icon-dots">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                  </div>
                </div>
                <div class="page-travel__signUp-step-content">
                  <span class="page-travel__heading page-travel__heading--step page-travel__heading--white">Prześlij zdjęcie</span>
                </div>
              </div>
              <div class="page-travel__signUp-step">
                <div class="page-travel__signUp-step-icon-wrapper">
                  <div class="page-travel__signUp-step-number-wrapper">
                    <div class="page-travel__signUp-step-number">
                      <span class="page-travel__heading page-travel__heading--step page-travel__heading--bolder">2</span>
                    </div>
                  </div>
                  <div class="page-travel__signUp-step-icon">
                    <img src="img/icons/text-box.png">
                  </div>
                  <div class="page-travel__signUp-step-icon-dots">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                  </div>
                </div>
                <div class="page-travel__signUp-step-content">
                  <span class="page-travel__heading page-travel__heading--step page-travel__heading--white">Odpowiedz na pytanie konkursowe</span>
                </div>
              </div>
              <div class="page-travel__signUp-step">
                <div class="page-travel__signUp-step-icon-wrapper">
                  <div class="page-travel__signUp-step-number-wrapper">
                    <div class="page-travel__signUp-step-number">
                      <span class="page-travel__heading page-travel__heading--step page-travel__heading--bolder">3</span>
                    </div>
                  </div>
                  <div class="page-travel__signUp-step-icon">
                    <img src="img/icons/voting.png">
                  </div>
                </div>
                <div class="page-travel__signUp-step-content">
                  <span class="page-travel__heading page-travel__heading--step page-travel__heading--white">Głosuj na najlepsze zgłoszenia</span>
                </div>
              </div>
            </div>
            <div id="formSection" class="page-travel__signUp-date">
              <span class="page-travel__heading page-travel__heading--white page-travel__steps">Czekamy na Wasze zgłoszenia </span>
              <span class="page-travel__heading page-travel__heading--white page-travel__heading--steps page-travel__heading--bolder">do 22 sierpnia!</span>
            </div>
          </div>
          <div class="page-travel__signUp-split-border"></div>
          <div class="page-travel__signUp-form-wrapper">
            <form action="" method="post" enctype="multipart/form-data" accept="image/*">
              <div class="page-travel__signUp-form-image page-travel__signUp-form-box-border">
                <img class="icon" src="img/icons/image.png">
                <div class="page-travel__signUp-form-image-button">
                  <input type="file" name="image" value="Wybierz zdjęcie" id="formChooseImage" required>
                  <a class="page-travel__button page-travel__button--yellow" id="formImageButton">Wybierz zdjęcie</a>
                </div>
                <img class="pre-image" id="formPreviewImage">
              </div>
              <div class="page-travel__signUp-form-msg-wrapper">
                <div class="page-travel__signUp-form-msg-header">
                  <span class="page-travel__heading page-travel__heading--white page-travel__steps">Pytanie konkursowe:</span>
                  <span class="page-travel__heading page-travel__heading--white page-travel__steps">dlaczego lubisz podróżować z Le Club AccorHotels?</span>
                </div>
                <textarea class="page-travel__heading page-travel__heading--white page-travel__heading--textbox page-travel__signUp-form-text-box page-travel__signUp-form-box-border" name="msg" placeholder="Wpisz tutaj swoją odpowiedź" required></textarea>
              </div>
              <div class="page-travel__signUp-form-send-wrapper">
                <div class="page-travel__signUp-form-checkbox-wrapper">
                  <input type="checkbox" name="statute" required>
                  <div class="page-travel__signUp-form-custom-checkbox">
                    <img src="img/icons/checkbox.png" alt="">
                  </div>
                  <span class="page-travel__heading page-travel__heading--statute page-travel__heading--white">Przeczytałem i zaakceptowałem regulamin konkursu</span>
                </div>
                <input class="page-travel__button page-travel__button--yellow" type="submit" value="Wyślij zgłoszenie">
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="page-travel__photos">
        <div class="page-travel__photos-wrapper">
          <div class="page-travel__photos-header">
            <h1 class="page-travel__heading">Zwiedź więcej niż 10 miejsc z innych klubowiczami i zobacz ich wspomnienia na zdjęciach!</h1>
          </div>
          <div class="page-travel__photos-gallery">
            <div class="page-travel__photos-photo-box contest-photo-box">
              <div class="page-travel__photos-photo-box-opened">
                <div class="page-travel__photos-photo-box-opened-bckg"></div>
                <div class="page-travel__photos-photo-box-opened-img-wrapper">
                  <div class="page-travel__photos-photo-box-opened-close"></div>
                  <img src="img/bottom-gallery-example2.png">
                </div>
              </div>
              <div class="page-travel__photos-photo-box-prev">
                <img src="img/bottom-gallery-example2.png" alt="">
                <div class="page-travel__photos-photo-box-mask">
                  <img src="img/icons/gallery-loupe.png" alt="">
                </div>
              </div>
            </div>
            <div class="page-travel__photos-photo-box contest-photo-box">
              <div class="page-travel__photos-photo-box-opened">
                <div class="page-travel__photos-photo-box-opened-bckg"></div>
                <div class="page-travel__photos-photo-box-opened-img-wrapper">
                  <div class="page-travel__photos-photo-box-opened-close"></div>
                  <img src="img/bottom-gallery-example.png">
                </div>
              </div>
              <div class="page-travel__photos-photo-box-prev">
                <img src="img/bottom-gallery-example.png" alt="">
                <div class="page-travel__photos-photo-box-mask">
                  <img src="img/icons/gallery-loupe.png" alt="">
                </div>
              </div>
            </div>
            <div class="page-travel__photos-photo-box contest-photo-box">
              <div class="page-travel__photos-photo-box-opened">
                <div class="page-travel__photos-photo-box-opened-bckg"></div>
                <div class="page-travel__photos-photo-box-opened-img-wrapper">
                  <div class="page-travel__photos-photo-box-opened-close"></div>
                  <img src="img/bottom-gallery-example2.png">
                </div>
              </div>
              <div class="page-travel__photos-photo-box-prev">
                <img src="img/bottom-gallery-example2.png" alt="">
                <div class="page-travel__photos-photo-box-mask">
                  <img src="img/icons/gallery-loupe.png" alt="">
                </div>
              </div>
            </div>
            <div class="page-travel__photos-photo-box contest-photo-box">
              <div class="page-travel__photos-photo-box-opened">
                <div class="page-travel__photos-photo-box-opened-bckg"></div>
                <div class="page-travel__photos-photo-box-opened-img-wrapper">
                  <div class="page-travel__photos-photo-box-opened-close"></div>
                  <img src="img/bottom-gallery-example.png">
                </div>
              </div>
              <div class="page-travel__photos-photo-box-prev">
                <img src="img/bottom-gallery-example.png" alt="">
                <div class="page-travel__photos-photo-box-mask">
                  <img src="img/icons/gallery-loupe.png" alt="">
                </div>
              </div>
            </div>
            <div class="page-travel__photos-photo-box contest-photo-box">
              <div class="page-travel__photos-photo-box-opened">
                <div class="page-travel__photos-photo-box-opened-bckg"></div>
                <div class="page-travel__photos-photo-box-opened-img-wrapper">
                  <div class="page-travel__photos-photo-box-opened-close"></div>
                  <img src="img/bottom-gallery-example.png">
                </div>
              </div>
              <div class="page-travel__photos-photo-box-prev">
                <img src="img/bottom-gallery-example.png" alt="">
                <div class="page-travel__photos-photo-box-mask">
                  <img src="img/icons/gallery-loupe.png" alt="">
                </div>
              </div>
            </div>
            <div class="page-travel__photos-photo-box contest-photo-box">
              <div class="page-travel__photos-photo-box-opened">
                <div class="page-travel__photos-photo-box-opened-bckg"></div>
                <div class="page-travel__photos-photo-box-opened-img-wrapper">
                  <div class="page-travel__photos-photo-box-opened-close"></div>
                  <img src="img/bottom-gallery-example2.png">
                </div>
              </div>
              <div class="page-travel__photos-photo-box-prev">
                <img src="img/bottom-gallery-example2.png" alt="">
                <div class="page-travel__photos-photo-box-mask">
                  <img src="img/icons/gallery-loupe.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <div class="page-travel__photos-button-wrapper">
            <a href="gallery.php" class="page-travel__button page-travel__button--blue">Przejdź do galerii</a>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer footer--extra-padding">
      <div class="footer__wrapper">
        <a href="index.php">
          <img src="img/ah-logo-transparent.png">
        </a>
        <a href="#" class="footer__ox-logo">
          <img src="img/ox-logo.png">
        </a>
      </div>
    </footer>
  </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
