<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>

    <section class="page-gallery">
      <div class="page-gallery__top">
        <div class="page-gallery__top-wrapper">
          <span class="page-gallery__header">Mamy dla Ciebie <span class="page-gallery__header--bolder">więcej niż 10</span> inspiracji na niezapomnianą podróż z Le Club AccorHotels. Zagłosuj na zdjęcie i opis, który najbardziej zainspirował Cię do kolejnej podróży!</span>
        </div>
      </div>
      <div class="page-gallery__gallery page-gallery__gallery--negative-top">
        <div class="page-gallery__gallery-wrapper">
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-1.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-2.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-3.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-4.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-5.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-6.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-1.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-1.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-2.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-3.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-4.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-5.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-6.png">
            </div>
          </a>
          <a href="gallery_single.php" class="page-gallery__gallery-image">
            <div class="page-gallery__gallery-image-mask">
              <img src="img/icons/gallery-loupe.png">
              <div class="page-gallery__gallery-image-mask-bottom">
                <img src="img/icons/user.png">
                <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">Anna Kowalska</span>
              </div>
            </div>
            <div class="page-gallery__gallery-image-like">
              <img src="img/icons/like.png">
              <span class="page-gallery__header page-gallery__header--white page-gallery__header--gallery-box">16</span>
            </div>
            <div class="page-gallery__gallery-image-box">
              <img src="img/gallery-ex-1.png">
            </div>
          </a>
        </div>
      </div>
      <div class="page-gallery__bottom">
        <span class="page-gallery__header">Tak wyglądały Wasze podróże z Le Club AccorHotels! Dziękujemy za każdą z nich! Zobaczcie <span class="page-gallery__header--bolder">więcej niż 10</span> korzyści ze wspólnych podróży, które dla Was przygotowaliśmy!</span>
        <div class="page-gallery__bottom-button">
          <a class="page-gallery__button page-gallery__button--yellow" href="#">Poznaj więcej niż 10 korzyści</a>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__wrapper">
        <a href="index.php">
          <img src="img/ah-logo-transparent.png">
        </a>
        <a href="#" class="footer__ox-logo">
          <img src="img/ox-logo.png">
        </a>
      </div>
    </footer>
  </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
