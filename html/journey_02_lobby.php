<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>

    <section class="page-lobby">
      <div class="page-lobby__background"></div>
      <div class="page-lobby__content">
        <a href="journey_03_hotelRoom.php" class="page-lobby__box page-lobby__box--card">
          <div class="page-lobby__box-copy page-lobby__box--arrow-right page-lobby__box--visible">
            <div class="page-lobby__box-copy-icon">
              <img src="img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Przejdź do swojego pokoju!</span>
          </div>
        </a>

        <a href="#" class="page-lobby__box page-lobby__box--terminal">
          <div class="page-lobby__box-copy page-lobby__box--arrow-bottom">
            <div class="page-lobby__box-copy-icon">
              <img src="img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Jako uczestnik Le Club AccorHotels korzystaj z ceny dla Klubowiczów i otrzymuj do 10% zniżki na swój pobyty!</span>
          </div>
        </a>

        <a href="#" class="page-lobby__box page-lobby__box--drink">
          <div class="page-lobby__box-copy page-lobby__box--arrow-bottom">
            <div class="page-lobby__box-copy-icon">
              <img src="img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Ciesz się wizytą w hotelu i rozpocznij swój pobyt z orzeźwiającym napojem w prezencie!</span>
          </div>
        </a>

        <a href="#" class="page-lobby__box page-lobby__box--keys">
          <div class="page-lobby__box-copy page-lobby__box--arrow-bottom">
            <div class="page-lobby__box-copy-icon">
              <img src="img/ah-logo-smll.png">
            </div>
            <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--copy-text">Z kartą LCAH zaoszczędzisz więcej niż 10 min na check-in! Zamelduj się on-line a w recepcji odbierz tylko klucze!</span>
          </div>
        </a>
      </div>
      <div class="page-lobby__arrow">
        <a href="journey_01_entrance.php" class="page-lobby__arrow-wrapper page-lobby__arrow-wrapper--left">
          <img src="img/icons/arrow-right-bckg.png">
          <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Do tyłu</span>
        </a>
        <a href="journey_03_hotelRoom.php" class="page-lobby__arrow-wrapper">
          <img src="img/icons/arrow-right-bckg.png">
          <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Przejdź dalej</span>
        </a>
      </div>
    </section>

  </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
