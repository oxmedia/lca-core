<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>
        <section class="page-hotelRoom">
            <div class="page-hotelRoom__background"></div>
            <div class="page-hotelRoom__content">
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--gift">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-bottom">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Prezent powitalny</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Cieszymy się, że podróżujesz z Le Club AccorHotels, dlatego podczas każdego pobytu w naszych hotelach mamy przygotowany dla Ciebie prezent powitalny.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--standards">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-left">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Wyższy standard pokoju</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Nasi klubowicze zasługują na wszystko, co najlepsze. Dlatego z nami wypoczywasz w pokoju o wyższym standardzie.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--wifi">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-bottom">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Darmowe wi-fi</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Ciesz się z nami darmowym wi-fi i załatw więcej niż 10 spraw bez wychodzenia z pokoju hotelowego.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--checkin">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-right">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Późne wymeldowanie i wczesne zameldowanie na życzenie</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Przyjeżdżasz wcześnie rano? Wyjeżdżasz późno w nocy? Z Le Club AccorHotels zameldujesz się i wymeldujesz na życzenie - kiedy tylko chcesz.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--executiveLounge">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-bottom">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">Dostęp do executive lounge</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Spędzisz więcej niż 10 minut w oczekiwaniu na samolot? Skorzystaj z executive lounge - z kartą LCAH masz dostęp do ponad 1000 prywatnych poczekalni na całym świecie.</span>
                    </div>
                </a>
                <a href="#" class="page-hotelRoom__box page-hotelRoom__box--lacollection">
                    <div class="page-hotelRoom__box-copy page-hotelRoom__box--arrow-right">
                        <div class="page-hotelRoom__box-copy-icon">
                            <img src="img/ah-logo-smll.png">
                        </div>
                        <h1 class="page-hotelRoom__heading page-hotelRoom__heading--copy-header page-hotelRoom__heading--white">La Collection</h1>
                        <span class="page-hotelRoom__heading page-hotelRoom__heading--white page-hotelRoom__heading--copy-text">Wejdź do świata La Collection i sprawdź na jakie produkty więcej niż 10 marek możesz wymienić punkty Rewards!</span>
                    </div>
                </a>
            </div>
            <div class="page-hotelRoom__arrow">
                <a href="journey_02_lobby.php" class="page-lobby__arrow-wrapper page-lobby__arrow-wrapper--left">
                    <img src="img/icons/arrow-right-bckg.png">
                    <span class="page-lobby__heading page-lobby__heading--white page-lobby__heading--arrow page-lobby__heading--bolder page-lobby__heading--upper">Do tyłu</span>
                </a>
                <a href="journey-04-laptopRoom.php" class="page-hotelRoom__arrow-wrapper">
                    <img src="img/icons/arrow-right-bckg.png">
                    <span class="page-hotelRoom__heading page-hotelRoom__heading--uppercase page-hotelRoom__heading--white page-hotelRoom__heading--arrow page-hotelRoom__heading--bolder page-hotelRoom__heading--upper">Przejdź dalej</span>
                </a>
            </div>
        </section>
    </body>
  <script type="text/javascript" src="js/app.js"></script>
</html>
