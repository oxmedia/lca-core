<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Le Club AH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.min.css">
  </head>
  <body>
    <!-- logo (style in _top-nav.scss) -->
    <div class="logo">
      <img src="img/ah-logo.png" alt="">
    </div>
    <!-- navigation -->
    <nav class="navbar navbar-expand-lg navbar-fixed-top" id="navbar">
      <div class="navbar__inset-shadow"></div>
      <a class="navbar-brand" href="index.php"><img src="img/icons/home-nav.png"></a>
      <button id="mobileCollapse" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="img/icons/hamburger.png">
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="page_travel.php">Konkurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Galeria prac</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="journey_01_entrance.php">Więcej niż 10!</a>
            </a>
          </li>
        </ul>
        <div class="navbar__social">
          <a href="https://www.facebook.com/leclubaccorhotels.polska/"> <img src="img/icons/social-fb.png"> </a>
          <a href="https://www.instagram.com/accorhotels_polska/"> <img src="img/icons/social-insta.png"> </a>
        </div>
      </div>
    </nav>

    <section class="page-events">
        <div class='page-events__top'>
            <span class="page-events__heading">Na jakie z minionych wydarzeń wybrałbyś się z nami?</span>
        </div>
        <div class='page-events__videos'>
            <a class='page-events__videos-button' href="journey-06-movie.php">
                <img src="img/ah-logo-smll-white.png">
                <span class="page-events__heading page-events__heading--mid page-events__heading--bolder page-events__heading--upper">Osobiście poczuj korzyści podróżowania z Le Club AccorHotel</span>
                <img src="img/icons/double-arrow.png">
            </a>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="img/chodakowska_thumb.png">
                <video class='page-events__videos-video-webm'>
                    <source src="videos/Elite Experiences - get fit with Le Club AccorHotels.webm" type="video/webm">
                </video>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Trening z Ewą Chodakowską</span>
                </div>
            </div>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="img/teatr_thumb.png">
                <video class='page-events__videos-video-webm'>
                    <source src="videos/teatr.webm" type="video/webm">
                </video>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Premiera opery "Poławiacze Pereł" <br> i kolacja w hotelu Sofitel Grand Sopot</span>
                </div>
            </div>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="img/sarsa_thumb.png">
                <video class='page-events__videos-video-webm'>
                    <source src="videos/sarsa.webm" type="video/webm">
                </video>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Koncert Sarsy</span>
                </div>
            </div>
            <div class='page-events__videos-video'>
                <img class='page-events__videos-video-thumb' src="img/castle_thumb.png">
                <video class='page-events__videos-video-webm'>
                    <source src="videos/Accor_Zamek_Krolewski.webm" type="video/webm">
                </video>
                <div class='page-events__videos-video-content'>
                    <span class="page-events__heading page-events__heading--white page-events__heading--bolder">Koncert na zamku</span>
                </div>
            </div>
        </div>
    </section>

</body>
<script type="text/javascript" src="js/app.js"></script>
</html>