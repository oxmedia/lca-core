/*-- OVERALL --*/

var windowWidth = $(window).width();

//-- top navigation
var $navbar = $("#navbar");
var $mobileCollapse = $("#mobileCollapse");

$mobileCollapse.on("click", function(){
  $navbar.toggleClass('navbar__mobile-collapsed')
})

function redirectOnMobile(){
  windowWidth = $(window).width();
  if (windowWidth < 992) { 
    window.location.href = "journey_01_entrance.php";
  }
}

/*-- HOME PAGE --*/

//-- home carousel
// var $homeCarousel = $("#homeCarousel");
//
// $homeCarousel.owlCarousel({
//     loop:true,
//     mouseDrag:false,
//     autoplay:true,
//     autoplayTimeout:15000,
//     margin:0,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         }
//     }
// });
//  /* keyboard nav */
//  $(document).on('keydown', function( event ) { //attach event listener
//     if(event.keyCode == 37) {
//         $homeCarousel.trigger('prev.owl')
//     }
//     if(event.keyCode == 39) {
//         $homeCarousel.trigger('next.owl')
//     }
// });
// /* scroll nav */
// $(window).on('mousewheel', function(e){
//   if (e.originalEvent.deltaY < 0) {
//     $homeCarousel.trigger('next.owl')
//   }
//   if (e.originalEvent.deltaY > 0) {
//     $homeCarousel.trigger('prev.owl')
//   }
// })

//-- home toggle popup
var $homePopup = $("#homePopup");
var $homePopup_bckg = $("#homePopup_bckg");
var $homePopup_box = $("#homePopup_box");
var $homePopup_gift = $("#homePopup_gift");
var $homePopup_switchOnBtn = $("#homePopup_switchOnBtn");
var $homePopup_switchOffBtn = $("#homePopup_switchOffBtn");
var $homeBox = $("#homeBox");

//params: 1)string: define whether we want to show or hide popup
function toggleHomePopup(action){
  if (action === "on") {
    $homePopup.addClass("active");
    $homeBox.removeClass("active");
    setTimeout(function(){
      $homePopup_box.addClass("active");
      $homePopup_gift.addClass("active");
    }, 5);
  }else if(action === "off"){
    $homePopup_box.removeClass("active");
    $homePopup_gift.removeClass("active");
    $homeBox.addClass("active");
    setTimeout(function(){
      $homePopup.removeClass("active");
    }, 330);
  }
}

//bind toggling function to DOM elements
$homePopup_switchOnBtn.on("click", function(){
  toggleHomePopup("on");
});
$homePopup_bckg.on("click", function(){
  toggleHomePopup("off");
})
$homePopup_switchOffBtn.on("click", function(){
  toggleHomePopup("off");
})
$(document).keyup(function(e) {
     if (e.keyCode == 27) {
       toggleHomePopup("off");
    }
});

/*-- TRAVEL PAGE --*/
//-- toggle hotel gallery box
//show
$(".gallery-box-wrapper").on("click", function(e){
  var $galleryPopup = $(this).find(".page-travel__hotel-preview-box-opened");
  if (!$galleryPopup.hasClass("active") && !$(e.target).hasClass("bckg") && !$(e.target).hasClass("close-box") && !$(e.target).hasClass("arrows-container") && !$(e.target).hasClass("gallery-arrow-left") && !$(e.target).hasClass("gallery-arrow-right")) {
    $galleryPopup.addClass("active");
  }
})
//hide
$(".gallery-box-wrapper").find(".bckg").on("click", function(){
  $(this).parent().removeClass("active");
})
$(".gallery-box-wrapper").find(".close-box").on("click", function(){
  console.log($(this).parent().parent());
  $(this).parent().parent().removeClass("active");
})

$(".gallery-arrow-right").on("click", function(){
  galleryNext($(this).parent().parent().parent())
})
$(".gallery-arrow-left").on("click", function(){
  galleryPrev($(this).parent().parent().parent())
})

function galleryNext(imageWrapper){
  imageWrapper.parent().find(".gallery-box-wrapper").each(function(){
    $(this).find(".page-travel__hotel-preview-box-opened").removeClass("active");
  })
  if (imageWrapper.next().length != 0) {
    imageWrapper.next().find(".page-travel__hotel-preview-box-opened").addClass('active');
  }else{
    imageWrapper.parent().find(".gallery-box-wrapper").eq(0).find(".page-travel__hotel-preview-box-opened").addClass('active');
  }
}
function galleryPrev(imageWrapper){
  imageWrapper.parent().find(".gallery-box-wrapper").each(function(){
    $(this).find(".page-travel__hotel-preview-box-opened").removeClass("active");
  })
  if (imageWrapper.prev().length != 0) {
    imageWrapper.prev().find(".page-travel__hotel-preview-box-opened").addClass('active');
  }else{
    imageWrapper.parent().find(".gallery-box-wrapper").last().find(".page-travel__hotel-preview-box-opened").addClass('active');
  }
}

//-- send image form
var $formChooseImage = $("#formChooseImage");
var $formImageButton = $("#formImageButton");
var $formPreviewImage = $("#formPreviewImage");

//choose image
$formChooseImage.on("change", function(e){
  var file = e.originalEvent.srcElement.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    $formPreviewImage.attr("src", reader.result);
    $formImageButton.text("Wybierz inne zdjęcie");
  }
  reader.readAsDataURL(file);
})


//-- toggle gallery box user images
//show
$(".contest-photo-box").on("click", function(e){
  var $opened = $(this).find('.page-travel__photos-photo-box-opened');
  if (!$opened.hasClass("active") && !$(e.target).hasClass("page-travel__photos-photo-box-opened-bckg") && !$(e.target).hasClass("page-travel__photos-photo-box-opened-close")) {

    $opened.addClass("active")
  }
})
//hide
$(".page-travel__photos-photo-box-opened-close").on("click", function(){
  var container = $(this).parent().parent();
  if (container.hasClass('active')) {
    $(container).removeClass("active");
  }
})
$(".page-travel__photos-photo-box-opened-bckg").on("click", function(){
  var container = $(this).parent();
  if (container.hasClass('active')) {
    $(container).removeClass("active");
  }
})


/*-- JOURNEY ENTRANCE PAGE --*/
var $hotelDoors = $("#hotelDoors");
var $hotelsSlideshow = $("#hotelsSlideshow");
var $hotelLightsOn = $("#hotelLightsOn");
var $benefitsCarousel = $("#benefitsCarousel");

$hotelsSlideshow.owlCarousel({
  responsive : {
    380:{
      items:1
    },
    480:{
      items:2
    },
    768:{
      items:3
    },
    1024:{
      items:5
    }
  },
  dots: false,
  autoplay: true,
  autoplayTimeout: 2000,
  autoplaySpeed: 2000,
  //autoplayHoverPause: true,
  loop: true,
  margin: 40,
  smartSpeed:450,
  fluidSpeed: 400,
  slideTransition: "linear"
})

$benefitsCarousel.owlCarousel ({
  responsive : {
    0:{
      items:1
    }
  },
  dots: false 
})

$hotelDoors.on("click", function(e){
  e.preventDefault();
  var url = $(this).attr("href");
  //lights on
  $hotelLightsOn.css("opacity", "1");
  //zoom
  setTimeout(function(){
    $hotelLightsOn.css("transition", "all 1s cubic-bezier(.25,.01,.55,1)");
    $hotelLightsOn.addClass("zoom");
  }, 330);
  setTimeout(function(){
    //next page
    window.location.href = url;
  }, 1330)
})


/*-- JOURNEY LOBBY PAGE --*/
if ($(".page-lobby").length) {
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });
}

/*-- JOURNEY LOBBY PAGE --*/
if ($(".page-hotelRoom").length) {
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });
}

/*-- JOURNEY LAPTOP ROOM PAGE --*/
if ($(".page-laptopRoom").length) {
  var $msgNotification = $("#msgNotification");
  var $infoBox = $("#infoBox");
  var $msgIcon = $("#msgIcon");
  var $closeBoxButton = $("#closeBoxButton");
  var $iframeSite = $("#iframeSite");

  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });

  $iframeSite.on("load", function(){
    setTimeout(function(){
      $msgNotification.addClass("active");
      setTimeout(function(){
        $msgNotification.removeClass("active");
        $msgIcon.addClass("active");
      }, 2000)
    }, 3500)
  
    $msgIcon.on("click", function(){
      $infoBox.addClass('active');
    })
  
    $closeBoxButton.on("click", function(){
      $infoBox.removeClass('active');
    })
  })
  
}

/*-- JOURNEY EVENTS PAGE --*/
if ($(".page-events").length) {
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });
  
  $(".page-events__videos-video").on("click", function(){
    var $video = $(this).find("video");

    $(this).find(".page-events__videos-video-thumb").remove(); 
    if(!$video.hasClass("playing")){
      $video[0].play();
      $video.addClass("playing")
    }else{ 
      $video[0].pause(); 
      $video.removeClass("playing")
    } 
  }) 
}  

/*-- JOURNEY VIDEO PAGE --*/
if ($(".page-movie").length) {
  redirectOnMobile();
  $(window).on("resize", function(){
    redirectOnMobile();
  });
}
 