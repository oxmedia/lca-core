const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const sourceMaps = require('gulp-sourcemaps');
const minifyCSS = require('gulp-cssmin');
const cleanCSS = require('gulp-clean-css');
const minifyJS = require('gulp-uglify');
const gutil = require('gulp-util');
const concat = require('gulp-concat');
const streamqueue  = require('streamqueue');
const imagemin = require('gulp-imagemin');

gulp.task('sass', function() {
	gulp.src('html/scss/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(gulp.dest('html/css'));
});

gulp.task('app-sass', function() {
	gulp.src('app/wp-content/themes/lca/scss/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(gulp.dest('app/wp-content/themes/lca/css'));
});

gulp.task('minify-css', () => {
  return gulp.src('html/css/main.css')
  	.pipe(rename({suffix: '.min'}))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('html/css'));
});

gulp.task('app-minify-css', () => {
  return gulp.src('app/wp-content/themes/lca/css/main.css')
  	.pipe(rename({suffix: '.min'}))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('app/wp-content/themes/lca/css'));
});

gulp.task('minify-js', function() {
	gulp.src('html/js/main.js')
		.pipe(rename({basename: "main", suffix: '.min'}))
		.pipe(minifyJS())
		.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
		.pipe(gulp.dest('html/js'));
});

gulp.task('app-minify-js', function() {
	gulp.src('app/wp-content/themes/lca/js/main.js')
		.pipe(rename({basename: "main", suffix: '.min'}))
		.pipe(minifyJS())
		.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
		.pipe(gulp.dest('app/wp-content/themes/lca/js'));
});

gulp.task('app-minify-img', function() {
	gulp.src('app/wp-content/themes/lca/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('app/wp-content/themes/lca/dist/img'))
});

gulp.task('compile-js', function() {
	var stream = streamqueue({ objectMode: true });
  stream.queue(gulp.src('html/js/libs/jquery.min.js'));
  stream.queue(gulp.src('html/js/libs/bootstrap.min.js'));
	stream.queue(gulp.src('html/js/libs/owl.carousel.min.js'));
  stream.queue(gulp.src('html/js/main.min.js'));
  return stream.done()
	.pipe(concat('app.js'))
  .pipe(gulp.dest('html/js/'));
});

gulp.task('app-compile-js', function() {
	var stream = streamqueue({ objectMode: true });
  stream.queue(gulp.src('app/wp-content/themes/lca/js/libs/jquery.min.js'));
  stream.queue(gulp.src('app/wp-content/themes/lca/js/libs/bootstrap.min.js'));
	stream.queue(gulp.src('app/wp-content/themes/lca/js/libs/owl.carousel.min.js'));
  stream.queue(gulp.src('app/wp-content/themes/lca/js/main.min.js'));
  return stream.done()
	.pipe(concat('app.js'))
  .pipe(gulp.dest('app/wp-content/themes/lca/js/'));
});

gulp.task('watch', function() {
	gulp.watch('html/scss/**/*.scss', ['sass']);
	gulp.watch('html/css/main.css', ['minify-css']);
	gulp.watch('html/js/main.js', ['minify-js', 'compile-js']);
});

gulp.task('app-watch', function() {
	gulp.watch('app/wp-content/themes/lca/scss/**/*.scss', ['app-sass']);
	gulp.watch('app/wp-content/themes/lca/css/main.css', ['app-minify-css']);
	gulp.watch('app/wp-content/themes/lca/js/main.js', ['app-minify-js', 'app-compile-js']);
});

gulp.task('default', ['sass', 'minify-css', 'minify-js', 'compile-js']);
gulp.task('app-default', ['app-sass', 'app-minify-css', 'app-minify-js', 'app-minify-img', 'app-compile-js']);
